import React from 'react';
import { screen, render, fireEvent } from '@testing-library/react';
import Register from '../../pages/register';

describe('Register page', () => {
  it('should properly render register page', async () => {
    const { container } = render('<Register />');
    expect(container).toBeTruthy();
    expect(container).toMatchSnapshot();
  });

  it('should display the Layout Component', async () => {
    render(<Register />);

    expect(screen.getByTestId('layout')).toBeInTheDocument();
  });

  it('should display header tag with "Register" text into it', async () => {
    render(<Register />);
    const header = screen.getByRole('heading');

    expect(header).toContainHTML('Register');
  });

  it('should display RegisterForm Component properly', async () => {
    render(<Register />);

    expect(screen.getByTestId('register-form')).toBeDefined();
  });

  it('should have "Register" as text on the submit button', async () => {
    render(<Register />);
    const button = screen.getByRole('button');

    expect(button.innerHTML).toEqual('Register');
  });

  describe('Event handling', () => {
    const itBindsInputValues = async (placeholder, value, expectedValue) => {
      render(<Register />);
      const input = screen.getByPlaceholderText(placeholder);

      fireEvent.change(input, { target: { value } });
      expect(input.value).toBe(expectedValue);
    };

    const itChangesButtonText = async (placeholder, value) => {
      render(<Register />);
      const input = screen.getByPlaceholderText(placeholder);
      const button = screen.getByRole('button');
      fireEvent.change(input, { target: { value } });
      expect(button.innerHTML).toBe('Submit');
    };
    const cases = [
      ['name', 'Type your name', 'John Doe', 'John Doe'],
      ['email', 'Type your email', 'email@test.com', 'email@test.com'],
      ['password', 'Type your password', 'password', 'password'],
    ];

    describe('bidirectional binding: should bind input value with what has been filled', () => {
      test.each(cases)('%s', (_name, placeholder, value, expectedValue) => {
        itBindsInputValues(placeholder, value, expectedValue);
      });
    });

    describe('text button changes from "Register" to "Submit" when', () => {
      test.each(cases)(
        '%s input value changes',
        (_name, placeholder, expectedValue) => {
          itChangesButtonText(placeholder, expectedValue);
        }
      );
    });
  });
});
