import React from 'react';
import { render } from '@testing-library/react';
import Header from './Header';

describe('Header Component', () => {
  it('should display properly', async () => {
    const { container } = render(<Header />);

    expect(container).toBeDefined();
    expect(container).toMatchInlineSnapshot(`
      <div>
        <link
          crossorigin="anonymous"
          data-testid="header"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
          rel="stylesheet"
        />
      </div>
    `);
  });
});
