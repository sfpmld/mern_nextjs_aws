import { useEffect } from 'react';
import NavBar from './NavBar';
import Header from './Header';
import {
  attachNProgressToRouterEvent,
  clearNProgressFromRouteEvent,
} from '../utils';

const clearRouterEventsWhenUnmount = () => clearNProgressFromRouteEvent();

const Layout = ({ children }) => {
  useEffect(() => {
    attachNProgressToRouterEvent();
    return clearRouterEventsWhenUnmount;
  }, []);

  return (
    <>
      <Header />
      <NavBar />
      <div data-testid="layout" className="container pt-5 pb-5">
        {children}
      </div>
    </>
  );
};

export default Layout;
