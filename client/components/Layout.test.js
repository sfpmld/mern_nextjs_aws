import React from 'react';
import { render } from '@testing-library/react';
jest.mock('../utils/NProgress');
import {
  attachNProgressToRouterEvent,
  clearNProgressFromRouteEvent,
} from '../utils/NProgress';
import Layout from './Layout';

describe('Layout Component', () => {
  const renderLayout = () => render(<Layout />);

  const layoutContainer = () => {
    const { container } = renderLayout();
    return container;
  };

  const existsComponentByTestId = id => {
    const { getByTestId } = renderLayout();
    const element = getByTestId(id);

    expect(element).toBeDefined();
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render properly', async () => {
    const container = layoutContainer();

    expect(container).toBeDefined();
    expect(container).toMatchSnapshot();
  });

  it('should render Header Component', async () => {
    existsComponentByTestId('header');
  });

  it('should render NavBar Component', async () => {
    existsComponentByTestId('navBar');
  });

  it('should attach NProgress to Router Event', async () => {
    renderLayout();
    expect(attachNProgressToRouterEvent).toHaveBeenCalledTimes(1);
  });

  it('should clear NProgress from Router Event when unmounting Layout Component', async () => {
    renderLayout();
    expect(clearNProgressFromRouteEvent).toHaveBeenCalledTimes(1);
  });
});
