import Link from 'next/link';

const NavBar = () => (
  <ul data-testid="navBar" className="nav nav-tabs bg-warning">
    <li className="nav-item">
      <Link href="/">
        <a data-testid="navBar-home" className="nav-link text-dark">
          Home
        </a>
      </Link>
    </li>
    <li className="nav-item">
      <Link href="/login">
        <a data-testid="navBar-login" className="nav-link text-dark">
          Login
        </a>
      </Link>
    </li>
    <li className="nav-item">
      <Link href="/register">
        <a data-testid="navBar-register" className="nav-link text-dark">
          Register
        </a>
      </Link>
    </li>
  </ul>
);

export default NavBar;
