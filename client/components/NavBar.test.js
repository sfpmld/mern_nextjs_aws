import React from 'react';
import { render, screen } from '@testing-library/react';
import NavBar from './NavBar.js';

describe('NavBar Component', () => {
  it('should display NavBar properly', async () => {
    const { container } = render(<NavBar />);

    expect(container).toBeDefined();
    expect(container).toMatchSnapshot();
    expect(container).toMatchSnapshot();
  });

  const existsNavLink = (title, path) => {
    render(<NavBar />);
    const link = screen.getByText(eval(`/${title}/i`));

    expect(link).toHaveAttribute('href', path);
  };

  const cases = [
    ['Home navLink with "/" path', 'Home', '/'],
    ['Login navLink with "/login" path', 'Login', '/login'],
    ['Register navLink with "/register" path', 'Register', '/register'],
  ];

  describe('should have a ', () => {
    test.each(cases)('%s', (_name, title, path) => {
      existsNavLink(title, path);
    });
  });
});
