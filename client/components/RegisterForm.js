const RegisterForm = ({ user, onChange, onSubmit, buttonText }) => {
  const { name, email, password } = user;
  return (
    <form data-testid="register-form" onSubmit={onSubmit}>
      <div className="form-group">
        <input
          className="form-control"
          type="text"
          name="name"
          data-testid="input-name"
          value={name}
          onChange={onChange}
          placeholder="Type your name"
        />
      </div>
      <div className="form-group">
        <input
          className="form-control"
          type="email"
          name="email"
          data-testid="input-email"
          value={email}
          onChange={onChange}
          placeholder="Type your email"
        />
      </div>
      <div className="form-group">
        <input
          className="form-control"
          type="password"
          name="password"
          data-testid="input-password"
          value={password}
          onChange={onChange}
          placeholder="Type your password"
        />
      </div>
      <div className="form-group">
        <button type="submit" className="btn btn-outline-warning">
          {buttonText}
        </button>
      </div>
    </form>
  );
};

export default RegisterForm;
