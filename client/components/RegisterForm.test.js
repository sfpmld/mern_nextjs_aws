import { screen, render, fireEvent } from '@testing-library/react';
import RegisterForm from './RegisterForm';

describe('RegisterForm Component', () => {
  const onChange = jest.fn();
  const onSubmit = jest.fn(e => e.preventDefault());

  const mockUser = {
    name: 'username',
    email: 'email@test.com',
    password: 'password',
  };
  const mockButtonText = 'Register';
  const renderRegisterForm = () => {
    render(
      <RegisterForm
        user={mockUser}
        onChange={onChange}
        onSubmit={onSubmit}
        buttonText={mockButtonText}
      />
    );
  };
  it('should display properly', async () => {
    renderRegisterForm();

    expect(screen.getByTestId('register-form')).toBeDefined();
  });

  const existsInput = async (type, placeholder) => {
    renderRegisterForm();
    const input = screen.getByPlaceholderText(placeholder);

    expect(input).toBeDefined();
    expect(input.tagName.toLowerCase()).toEqual('input');
    expect(input.type.toLowerCase()).toEqual(type);
  };

  const cases = [
    ['name', 'text', 'Type your name'],
    ['email', 'email', 'Type your email'],
    ['password', 'password', 'Type your password'],
  ];

  describe('should display inputs: ', () => {
    test.each(cases)('%s of type "%s"', (_name, type, placeholder) => {
      existsInput(type, placeholder);
    });
  });
  it(`should have a button with "${mockButtonText}" as default text`, async () => {
    renderRegisterForm();
    const button = screen.getByRole('button');

    expect(button.innerHTML).toBe(mockButtonText);
  });
  describe('Event handling', () => {
    const cases = [
      ['name', 'Type your name', 'Jane Doe', 'Jane Doe'],
      ['email', 'Type your email', 'jane@test.com', 'jane@test.com'],
      ['password', 'Type your password', 'new-password', 'new-password'],
    ];

    const fillInput = async (_name, placeholder, value) => {
      renderRegisterForm();
      const input = screen.getByPlaceholderText(placeholder);

      fireEvent.change(input, { target: { value } });
    };

    describe('should call changeHandler after having change one input value', () => {
      test.each(cases)('%s', (name, value, expectedValue) => {
        fillInput(name, value, expectedValue);
        expect(onChange).toHaveBeenCalled();
      });
    });

    describe('Submit Handler', () => {
      it('should call submitHandler after having clicked on submit button', async () => {
        renderRegisterForm();
        const button = screen.getByRole('button');
        fireEvent.click(button);

        expect(onSubmit).toHaveBeenCalled();
      });
    });
  });
});
