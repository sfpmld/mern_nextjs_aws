import { getFromEnv } from '../utils';

export const API_URL = getFromEnv('NEXT_PUBLIC_API_URL');
