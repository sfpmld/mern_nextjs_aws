import { useState } from 'react';
import Layout from '../components/Layout';
import RegisterForm from '../components/RegisterForm';
import axios from 'axios';
import { API_URL } from '../constants';

const register = () => {
  const [state, setState] = useState({
    name: '',
    email: '',
    password: '',
    error: false,
    success: false,
    buttonText: 'Register',
  });

  const { name, email, password, error, success, buttonText } = state;

  const handleChange = event => {
    const { name, value } = event.target;
    setState({
      ...state,
      [name]: value,
      error: false,
      success: false,
      buttonText: 'Submit',
    });
  };

  const handleSubmit = event => {
    event.preventDefault();
    axios
      .post(`${API_URL}/register`, {
        name,
        email,
        password,
      })
      .then(response => console.log(response))
      .catch(error => console.log(error));
  };

  return (
    <Layout>
      <div className="col-md-6 offset-md-3">
        <h1>Register</h1>
        <br />
        <RegisterForm
          user={(name, email, password)}
          onChange={handleChange}
          onSubmit={handleSubmit}
          buttonText={buttonText}
        />
      </div>
    </Layout>
  );
};

export default register;
