import Router from 'next/router';
import NProgress from 'nprogress';

export const attachNProgressToRouterEvent = () => {
  Router.events.on('routeChangeStart', url => NProgress.start(url));
  Router.events.on('routeChangeComplete', () => NProgress.done());
  Router.events.on('routeChangeError', url => NProgress.start(url));
};

export const clearNProgressFromRouteEvent = () => {
  Router.events.off('routeChangeStart');
  Router.events.off('routeChangeComplete');
  Router.events.off('routeChangeError');
};

