import Router from 'next/router';
import {
  attachNProgressToRouterEvent,
  clearNProgressFromRouteEvent,
} from './NProgress';
import NProgress from 'nprogress';
jest.mock('nprogress');

describe('Utils functions to attach NProgress to Router', () => {
  Router.events.off = jest.fn();
  beforeEach(() => {
    attachNProgressToRouterEvent();
  });

  describe('attachNProgressToRouterEvent', () => {
    it('should call NProgress.start(url) when "routeChangeStart" event', async () => {
      Router.events.emit('routeChangeStart');

      expect(NProgress.start).toHaveBeenCalled();
    });
    it('should call NProgress.done(url) when "routeChangeComplete" event', async () => {
      Router.events.emit('routeChangeComplete');

      expect(NProgress.done).toHaveBeenCalled();
    });
    it('should call NProgress.start(url) when "routeChangeError" event', async () => {
      Router.events.emit('routeChangeError');

      expect(NProgress.start).toHaveBeenCalled();
    });
  });

  describe('clearNProgressFromRouteEvent', () => {
    it('should call Router.events.off(event) when execute ClearNProgressFromRouteEvent function', async () => {
      clearNProgressFromRouteEvent();

      expect(Router.events.off).toHaveBeenCalledWith('routeChangeStart');
      expect(Router.events.off).toHaveBeenCalledWith('routeChangeComplete');
      expect(Router.events.off).toHaveBeenCalledWith('routeChangeError');
    });
  });
});
