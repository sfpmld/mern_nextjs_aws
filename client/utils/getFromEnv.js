export const getFromEnv = varName => {
  const envVar = process.env[varName];
  // if (!envVar) throw new Error(varName);
  if (!envVar) console.log(`${varName} env var is missing`);
  return envVar;
};
