import { getFromEnv } from './getFromEnv';

describe('getFromEnv utility function', () => {
  const varName = 'TEST_VAR';
  const otherVarName = 'OTHER_TEST_VAR';

  global.console.log = jest.fn();

  it('should get the env variable values from environment file or process', async () => {
    process.env[varName] = 'test';
    const env = getFromEnv(varName);

    expect(env).toEqual('test');
  });
  it('should console.log if env var searched is missing', async () => {
    getFromEnv(otherVarName);

    expect(global.console.log).toHaveBeenCalled();
  });
});
