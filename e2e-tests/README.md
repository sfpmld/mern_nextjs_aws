# e2e-tests

The aim of this repo is to provide end to end (e2e) tests for our application. [Cypress.io](https://www.cypress.io/) is the choosen testing framework.

## Local Setup

To be able to run these tests locally you will need to run these commands:

- into backend directory:
    yarn debug
- into frontend directory:
    yarn dev
- into e2e-test directory:
    yarn dev:cy:open

### Minimal Required Dependencies

- [node](https://github.com/tj/n)
- [yarn](https://yarnpkg.com/getting-started/install)
- [docker](https://www.docker.com/)

### Minimal Requirements for AWS

You need the following credentials:

- an access key ID
- a secret access key
- a default region name
