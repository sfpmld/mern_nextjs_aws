import newUser from '../../fixtures/newUser.js';

describe('Registration page', () => {
	const loadHomePage = async () => {
		const CLIENT_URL = await cy.getFromEnv('CLIENT_URL');
		const LOADING_URL_TIMEOUT = await cy.getFromEnv('LOADING_URL_TIMEOUT');

		cy.visit(CLIENT_URL, { timeout: LOADING_URL_TIMEOUT });
	};

	beforeEach(() => {
		loadHomePage();
	});

	it('should load home page', () => {
		cy.getDataTestId('navBar').should('be.visible');
	});

	it('click event on register nav link should lead to the register page', () => {
		cy.getDataTestId('navBar-register').click();
	});
});

describe('Registration form', () => {
	const loadRegistrationPage = async () => {
		const CLIENT_URL = await cy.getFromEnv('CLIENT_URL');
		const LOADING_URL_TIMEOUT = await cy.getFromEnv('LOADING_URL_TIMEOUT');

		cy.visit(`${CLIENT_URL}/register`, { timeout: LOADING_URL_TIMEOUT });
	};

	before(() => {
		loadRegistrationPage();
	});

	it('registration inputs should be visible on registration page', () => {
		cy.shouldBeVisible('input-name');
		cy.shouldBeVisible('input-email');
		cy.shouldBeVisible('input-password');
	});
});

describe('When registration form is properly filled', () => {
	const loadRegistrationPage = async () => {
		const CLIENT_URL = await cy.getFromEnv('CLIENT_URL');
		const LOADING_URL_TIMEOUT = await cy.getFromEnv('LOADING_URL_TIMEOUT');

		cy.visit(`${CLIENT_URL}/register`, { timeout: LOADING_URL_TIMEOUT });
	};

	before(() => {
		loadRegistrationPage();
	});

	it('should receive 200 status code an a token string via the response body when submitting register form', () => {
		cy.log('filling form input');
		cy.fillInput('input-name', newUser.name);
		cy.fillInput('input-email', newUser.email);
		cy.fillInput('input-password', newUser.password);

		cy.server();
		cy.route({
			method: 'POST',
			url: /register/,
		}).as('register');

		cy.log('submitting form');
		cy.get('[type="submit"]').click();

		cy.log(
			`should receive 200 status code an a message field containing ${newUser.email} via the response body`
		);
		cy.wait('@register').then(xhr => {
			expect(xhr.status).toEqual(200);
			expect(xhr.response.body).toEqual(
				expect.objectContaining({
					message: expect.stringContaining(newUser.email),
				})
			);
		});
	});
});

describe('When registration form is not properly filled with user info (missing a mandatory user info)', () => {
	const loadRegistrationPage = async () => {
		const CLIENT_URL = await cy.getFromEnv('CLIENT_URL');
		const LOADING_URL_TIMEOUT = await cy.getFromEnv('LOADING_URL_TIMEOUT');

		cy.visit(`${CLIENT_URL}/register`, { timeout: LOADING_URL_TIMEOUT });
	};

	before(() => {
		loadRegistrationPage();
	});

	it('should receive 422 status code with an error message string when submitting registration form with missing user infos', () => {
		cy.log('filling form input');
		cy.fillInput('input-email', newUser.email);
		cy.fillInput('input-password', newUser.password);

		cy.server();
		cy.route({
			method: 'POST',
			url: /register/,
		}).as('register');

		cy.log('submitting form');
		cy.get('[type="submit"]').click();

		cy.log('should receive 422 status code an a error message string');
		cy.wait('@register').then(xhr => {
			expect(xhr.status).toEqual(422);
			expect(xhr.response.body.error).toEqual(
				expect.objectContaining({
					message: expect.stringContaining('Validation failed'),
					status: 422,
				})
			);
		});
	});
});
