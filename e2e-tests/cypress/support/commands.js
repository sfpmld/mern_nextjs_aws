// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

export const getFromEnv = async varName => {
	const envVar = await Cypress.env(varName);
	if (!envVar) console.log(`${varName} env var is missing`);
	return envVar;
};

export const getDataTestId = tag => cy.get(`[data-testid=${tag}`);

export const shouldBeVisible = dataTestId => {
	cy.getDataTestId(dataTestId).should('be.visible');
};

export const fillInput = (dataTestId, value) => {
	cy.getDataTestId(dataTestId).clear();
	cy.getDataTestId(dataTestId).type(value);
};
