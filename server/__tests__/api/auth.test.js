const { makeFakeUser, request } = require('../helpers');
const { UserDb } = require('../../src/data-access');
const { App } = require('../../src/helpers/express');
const { createApp } = require('../../src/server');
const { API_PREFIX } = require('../../src/constants');
const {
  HTTP_OK,
  HTTP_BAD_REQUEST,
  HTTP_ALREADY_EXIST_MESSAGE,
} = require('../../src/constants');
const {
  dbDisconnect,
  dbTruncate,
} = require('../../src/data-access/db/connection');

jest.mock('../../src/services', () => {
  const original = jest.requireActual('../../src/services');
  return {
    ...original,
    sendEmail: {
      send: jest.fn(() => 'email sent'),
    },
  };
});
jest.mock('../../src/helpers/logger');

describe('Authentication', () => {
  let app;
  beforeAll(async () => {
    app = await createApp({ App });
  });
  beforeEach(async () => {
    await dbTruncate();
  });
  afterAll(async () => {
    await dbDisconnect();
  });

  describe('Register Controller', () => {
    describe('When New User request register', () => {
      let res;
      beforeAll(async () => {
        res = await request(app)
          .post(`${API_PREFIX}/register`)
          .send({
            ...makeFakeUser(),
          });
      });

      it('should register with Status 200', async () => {
        expect(res.statusCode).toBe(HTTP_OK);
      });
      it('should return a body json object with a message field', async () => {
        expect(res.body.message).toBeTruthy();
      });
      it('this message field should be a string', async () => {
        expect(typeof res.body.message).toBe('string');
      });
    });

    describe('When User Already Exist', () => {
      let user;
      let res;
      beforeAll(async () => {
        user = makeFakeUser();
        UserDb.insert({
          ...user,
        });
        res = await request(app)
          .post(`${API_PREFIX}/register`)
          .send({
            ...user,
          });
      });

      it(`should not register but would send back status ${HTTP_BAD_REQUEST}`, async () => {
        expect(res.statusCode).toBe(HTTP_BAD_REQUEST);
      });
      it('should return a body json object with an error field', async () => {
        expect(res.body.error).toBeTruthy();
      });
      it(`body error.message field should correspond to "${HTTP_ALREADY_EXIST_MESSAGE}"`, async () => {
        expect(res.body.error.message).toBe(HTTP_ALREADY_EXIST_MESSAGE);
      });
      it(`body error.status should correspond to "${HTTP_BAD_REQUEST}"`, async () => {
        expect(res.body.error.status).toBe(HTTP_BAD_REQUEST);
      });
    });
  });
});
