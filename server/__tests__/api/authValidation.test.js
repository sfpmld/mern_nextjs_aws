const { makeFakeUser, request } = require('../helpers');
const { App } = require('../../src/helpers/express');
const { createApp } = require('../../src/server');
const { API_PREFIX } = require('../../src/constants');
const { HTTP_UNPROCESSABLE_ENTITY, HTTP_OK } = require('../../src/constants');
const { dbDisconnect } = require('../../src/data-access/db/connection');

jest.mock('../../src/services', () => {
  const original = jest.requireActual('../../src/services');
  return {
    ...original,
    sendEmail: {
      send: jest.fn(() => 'email sent'),
    },
  };
});
jest.mock('../../src/helpers/logger');

describe('authValidator', () => {
  let app;
  beforeAll(async () => {
    app = await createApp({ App });
  });
  afterAll(async () => {
    await dbDisconnect();
  });

  describe('userRegisterValidator', () => {
    it("should throw when users data don't contain name", async () => {
      const res = await request(app)
        .post(`${API_PREFIX}/register`)
        .send({
          ...makeFakeUser({ name: undefined }),
        });
      expect(res.statusCode).toBe(HTTP_UNPROCESSABLE_ENTITY);
    });

    it('should not throw when required fields are properly filled', async () => {
      return await request(app)
        .post(`${API_PREFIX}/register`)
        .send({
          ...makeFakeUser(),
        })
        .then(res => {
          expect(res.statusCode).toBe(HTTP_OK);
        });
    });
  });
});
