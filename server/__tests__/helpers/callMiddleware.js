const { makeExpressCallback } = require('../../src/middlewares');

module.exports.callMiddleware = (middleware, req, res, next) => {
  const expressMiddleware = makeExpressCallback(middleware);
  return expressMiddleware(req, res, next);
};
