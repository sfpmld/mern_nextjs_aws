const { callMiddleware } = require('./callMiddleware');
const middlewares = require('../../src/middlewares');
jest.mock('../../src/middlewares');
const mockMakeExpressCallback = middlewares.makeExpressCallback;
jest.mock('../../src/helpers/logger');

describe('callMiddleware test helper', () => {
  const mockMiddleware = jest.fn();
  const mockRequestGet = jest.fn(type => {
    let header;
    switch (type) {
      case 'Content-Type':
        header = 'content-type';
        break;
      case 'referer':
        header = 'referer';
        break;
      case 'User-Agent':
        header = 'user-agent';
        break;
    }
    return header;
  });
  const mockRequest = jest.fn();
  mockRequest.get = mockRequestGet;
  const mockResponse = jest.fn();
  mockResponse.set = jest.fn();
  mockResponse.status = jest.fn(() => this);
  const mockNext = jest.fn();
  const httpRequest = {
    path: 'path',
    originalUrl: 'originalUrl',
    method: 'http method',
    params: 'params',
    query: 'query',
    body: 'body',
    req: mockRequest,
    res: mockResponse,
    next: mockNext,
    headers: {
      'Content-Type': 'content-type',
      Referer: 'referer',
      'User-Agent': 'user-agent',
    },
  };
  Object.assign(mockRequest, httpRequest);

  beforeEach(async () => {});
  afterEach(async () => {
    mockMiddleware.mockClear();
    mockRequest.mockClear();
    mockResponse.mockClear();
    mockNext.mockClear();
    mockMakeExpressCallback;
  });

  it('should call makeExpressCallback factory', async () => {
    mockMakeExpressCallback.mockImplementationOnce(() => () => jest.fn());
    await callMiddleware(
      () =>
        new Promise((resolve, _) =>
          resolve({
            statusCode: 200,
            body: 'body',
          })
        ),
      mockRequest,
      mockResponse,
      mockNext
    );
    expect(mockMakeExpressCallback).toHaveBeenCalled();
  });
  it('should call a given middleware with req, res and next function as arguments', async () => {
    mockMakeExpressCallback.mockImplementationOnce(() => mockMiddleware);
    mockMiddleware.mockImplementationOnce(() => 'ok');
    await callMiddleware(mockMiddleware, mockRequest, mockResponse, mockNext);

    expect(mockMiddleware).toHaveBeenCalledWith(
      mockRequest,
      mockResponse,
      mockNext
    );
  });
});
