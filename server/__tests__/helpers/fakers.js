const faker = require('faker');

const removeNullifiedOrUndefinedFields = (obj, filter) => {
  if (!filter) return;
  const newObj = { ...obj };
  Object.keys(newObj).forEach(key => {
    if (!newObj[key]) {
      delete newObj[key];
    }
  });
  return newObj;
};

module.exports = {
  makeFakeUser: override => {
    let result = {
      username: faker.internet.userName(),
      name: faker.name.firstName() + faker.name.lastName(),
      email: faker.internet.email(),
      password: faker.internet.password(),
    };
    result = { ...result, ...override };

    return removeNullifiedOrUndefinedFields(result, override) || result;
  },
};
