const { makeFakeUser } = require('.');
jest.mock('../../src/helpers/logger');

describe('makeFakeUser helpers', () => {
  it('should generate fakes user infos that are required for user registration', () => {
    const expectedField = ['username', 'name', 'email', 'password'];
    const fakeUserInfos = makeFakeUser();

    expect(Object.keys(fakeUserInfos)).toMatchObject(expectedField);
  });

  it('should override fields that are given as params', async () => {
    const fakeUserInfos = makeFakeUser({ username: 'test' });

    expect(fakeUserInfos.username).toEqual('test');
  });

  it('should remove fields that are given as params with null value', async () => {
    const expectedField = ['name', 'email', 'password'];
    const fakeUserInfos = makeFakeUser({ username: null });

    expect(Object.keys(fakeUserInfos)).toMatchObject(expectedField);
  });

  it('should remove fields that are given as params with undefined value', async () => {
    const expectedField = ['name', 'email', 'password'];
    const fakeUserInfos = makeFakeUser({ username: undefined });

    expect(Object.keys(fakeUserInfos)).toMatchObject(expectedField);
  });
});
