const fakers = require('./fakers');
const { callMiddleware } = require('./callMiddleware');
const { loadEnvVarsInTestEnv } = require('./loadEnvVars');
const { request } = require('./request');

module.exports = {
  ...fakers,
  callMiddleware,
  loadEnvVarsInTestEnv,
  request,
};
