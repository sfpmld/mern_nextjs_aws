const dotenv = require('dotenv');

module.exports = {
  loadEnvVarsInTestEnv: () => {
    dotenv.config({ path: '.env.test' });
  },
};
