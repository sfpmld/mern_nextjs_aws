const { makeFakeUser } = require('../helpers');

const { dbDisconnect } = require('../../src/data-access');
const { UserDb } = require('../../src/data-access');
jest.mock('../../src/helpers/logger');

describe('User Model', () => {
  afterAll(async () => {
    await dbDisconnect();
    await UserDb.removeAll();
  });

  it('should encrypt given password before saving it in database', async () => {
    const mockUser = makeFakeUser();
    const newUser = await UserDb.insert(mockUser);

    expect(newUser.password).toBeTruthy();
    expect(newUser.password).not.toBe(mockUser.password);
  });
});
