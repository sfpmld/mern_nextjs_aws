const { makeFakeUser } = require('../helpers');
const { dbDisconnect } = require('../../src/data-access');
const { UserDb } = require('../../src/data-access');
jest.mock('../../src/helpers/logger');

describe('UserDB repository', () => {
  beforeEach(async () => {
    await UserDb.removeAll();
  });
  afterAll(async () => {
    await UserDb.removeAll();
    await dbDisconnect();
  });

  it('should provide given methods to interact with database', async () => {
    expect(UserDb).toMatchInlineSnapshot(`
      Object {
        "findAll": [Function],
        "findOne": [Function],
        "findOneByEmail": [Function],
        "findOneById": [Function],
        "insert": [Function],
        "remove": [Function],
        "removeAll": [Function],
        "update": [Function],
      }
    `);
  });

  it('should save user infos in database (insert method)', async () => {
    const mockUser = makeFakeUser();
    const newUser = await UserDb.insert(mockUser);

    expect(newUser).toBeTruthy();
    expect(newUser.username).toEqual(mockUser.username.toLowerCase());
    expect(newUser.name).toEqual(mockUser.name);
    expect(newUser.email).toEqual(mockUser.email);
  });

  it('should find a user that would have been created previously (findOne method)', async () => {
    const mockUser = makeFakeUser();
    await UserDb.insert(mockUser);
    const user = await UserDb.findOne({ username: mockUser.username });

    expect(user).toBeTruthy();
  });

  it('should find users by email', async () => {
    const mockUser = makeFakeUser();
    await UserDb.insert(mockUser);
    const user = await UserDb.findOneByEmail(mockUser.email);

    expect(user).toBeTruthy();
  });

  it('should find several users that would have been created previously (findAll method)', async () => {
    const mockUsers = [
      makeFakeUser(),
      makeFakeUser(),
      makeFakeUser(),
      makeFakeUser(),
    ];
    await Promise.all(mockUsers.map(user => UserDb.insert(user))).then(
      async () => {
        const users = await UserDb.findAll({});
        const resultUsernames = users.map(user => user.username);
        const expectedUsernames = mockUsers.map(user =>
          user.username.toLowerCase()
        );
        expect(expectedUsernames).toEqual(resultUsernames);
        expect(users.length).toBe(mockUsers.length);
      }
    );
  });

  it('should update field of a given existing user (update method)', async () => {
    const mockUser = makeFakeUser();
    const userData = await UserDb.insert(mockUser);

    let updatedUser = await UserDb.update(userData.id, { username: 'updated' });

    expect(updatedUser.username).not.toEqual(userData.username);
  });

  it('should remove a given created user (remove method)', async () => {
    const mockUser = makeFakeUser();
    const insertedUser = await UserDb.insert(mockUser);
    await UserDb.remove(insertedUser._id);

    expect(await UserDb.findOne(insertedUser._id)).toBeNull();
  });

  it('should remove all created users present in the user collection (removeAll method)', async () => {
    let users;
    const mockUsers = [
      makeFakeUser(),
      makeFakeUser(),
      makeFakeUser(),
      makeFakeUser(),
    ];
    await Promise.all(mockUsers.map(user => UserDb.insert(user))).then(
      async () => {
        users = await UserDb.findAll({});
        await UserDb.removeAll();
        expect(users.length).toBeGreaterThan(0);
        users = await UserDb.findAll({});
        expect(users.length).toBe(0);
      }
    );
  });
});
