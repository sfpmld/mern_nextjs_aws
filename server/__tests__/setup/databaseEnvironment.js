// my-custom-environment
const NodeEnvironment = require('jest-environment-node');
const dotenv = require('dotenv');
dotenv.config();

class databaseEnvironment extends NodeEnvironment {
  constructor(config, context) {
    super(config, context);
    this.testPath = context.testPath;
    this.docblockPragmas = context.docblockPragmas;
  }

  async setup() {
    this.global.DB_URL = process.env.DB_URL;
    await super.setup();
  }

  async teardown() {
    await super.teardown();
  }

  runScript(script) {
    return super.runScript(script);
  }
}

module.exports = databaseEnvironment;
