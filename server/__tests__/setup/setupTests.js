const dotenv = require('dotenv');
const { getFromEnv } = require('../../src/helpers');
const server = require('../../src/server');

module.exports = async () => {
  // Load .env.test environment variables
  const result = dotenv.config({ path: '.env.test' });
  if (result.error) {
    throw result.error;
  }
  const service = getFromEnv('SERVICE_NAME');
  const port = getFromEnv('PORT');
  const env = getFromEnv('ENV');

  global.server = server;
  global.service = service;
  global.httpServer = await server.start({ service, port, env });
};
