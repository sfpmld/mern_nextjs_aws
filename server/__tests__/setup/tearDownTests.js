module.exports = async () => {
  const server = global.server;
  const service = global.service;

  await server.stop({ server: global.httpServer, service });
};
