const { getFromEnv } = require('../helpers');

const SERVICE_NAME = getFromEnv('SERVICE_NAME');
const SERVICE_VERSION = getFromEnv('SERVICE_VERSION');
const ENVIRONMENT = getFromEnv('ENVIRONMENT');

const PORT = getFromEnv('PORT');
const DB_URL = getFromEnv('DB_URL');
const ENV = getFromEnv('ENV');
const API_PREFIX = getFromEnv('API_PREFIX');

const AWS_SES_VERSION = getFromEnv('AWS_SES_VERSION');

const EMAIL_FROM = getFromEnv('EMAIL_FROM');
const EMAIL_TO = getFromEnv('EMAIL_TO');
const ACTIVE_SEND_EMAIL = getFromEnv('ACTIVE_SEND_EMAIL');

const JWT_ACCOUNT_ACTIVATION = getFromEnv('JWT_ACCOUNT_ACTIVATION');
const JWT_ACCOUNT_ACTIVATION_EXPIRES = getFromEnv(
  'JWT_ACCOUNT_ACTIVATION_EXPIRES'
);

module.exports = {
  SERVICE_NAME,
  SERVICE_VERSION,
  ENVIRONMENT,
  PORT,
  DB_URL,
  ENV,
  API_PREFIX,
  AWS_SES_VERSION,
  EMAIL_FROM,
  EMAIL_TO,
  JWT_ACCOUNT_ACTIVATION,
  JWT_ACCOUNT_ACTIVATION_EXPIRES,
  ACTIVE_SEND_EMAIL,
};
