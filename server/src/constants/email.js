module.exports = {
  emailCharset: 'UTF-8',
  accountActivationSubject: 'Activation acount email',
  accountActivationMessage: 'Click the link to activate your account',
};
