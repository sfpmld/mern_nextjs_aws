module.exports = {
  // status code
  HTTP_OK: 200,
  HTTP_BAD_REQUEST: 400,
  HTTP_UNPROCESSABLE_ENTITY: 422,
  HTTP_INTERNAL_ERROR: 500,
  // messages
  HTTP_INTERNAL_ERROR_MESSAGE: 'Interval server error.',
  HTTP_ALREADY_EXIST_MESSAGE: 'User already exists',
};
