const config = require('./config');
const httpStatus = require('./httpStatus');
const email = require('./email');
const errorMessages = require('./errorMessages');
const messages = require('./messages');

module.exports = {
  ...config,
  ...httpStatus,
  ...email,
  ...errorMessages,
  ...messages,
};
