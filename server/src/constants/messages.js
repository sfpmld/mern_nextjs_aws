module.exports = {
  EMAIL_SENT: 'Email sent successfully',
  EMAIL_NOT_SENT: 'Sorry, email has not been sent successfully',
  JSON_EMAIL_SENT_TEMPLATE:
    'Email has been sent to {{ email }}, Follow the instructions to complete your registration',
};
