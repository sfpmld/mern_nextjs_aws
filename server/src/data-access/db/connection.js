const mongoose = require('mongoose');

const { ENV, DB_URL } = require('../../constants');

let connection;

const dbConnect = async function (MONGODB_URL = ENV ? DB_URL : global.DB_URL) {
  try {
    connection =
      connection ||
      (await mongoose.connect(MONGODB_URL, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify: false,
      }));
    console.log('Connected to MongoDB');
  } catch (err) {
    console.log('MongoDB failed to connect!', err);
  }
  return connection;
};

const dbTruncate = async function () {
  if (mongoose.connection.readyState !== 0) {
    const { collections } = mongoose.connection;

    const promises = Object.keys(collections).map(collection =>
      mongoose.connection.collection(collection).deleteMany({})
    );

    await Promise.all(promises);
  }
};

const dbDisconnect = async function () {
  return await mongoose.connection.close();
};

module.exports = {
  dbConnect,
  dbTruncate,
  dbDisconnect,
};
