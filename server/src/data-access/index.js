const { dbConnect, dbDisconnect } = require('./db/connection');

const { makeUserDb } = require('./repositories');

const makeDb = async () => {
  await dbConnect();
};

const UserDb = makeUserDb({ makeDb });

module.exports = Object.freeze({
  UserDb,
  dbConnect,
  dbDisconnect,
});
