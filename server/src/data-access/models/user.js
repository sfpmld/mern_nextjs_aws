const mongoose = require('mongoose');
const { tryAsyncAwait } = require('../../helpers');
const { hashPassword } = require('../../helpers/security/hashPassword');
const { makeSalt } = require('../../helpers/security/makeSalt');

mongoose.set('useCreateIndex', true);

// Schema
const userSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
      trim: true,
      max: 12,
      unique: true,
      index: true,
      lowercase: true,
    },
    name: {
      type: String,
      required: true,
      trim: true,
      max: 32,
    },
    email: {
      type: String,
      required: true,
      trim: true,
      max: 32,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    salt: String,
    role: {
      type: String,
      default: 'subscriber',
    },
    resetPasswordLink: {
      data: String,
      default: '',
    },
  },
  { timestamps: true }
);

// Virtual Field to hash password before saving in database
userSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();
  // generate salt
  this.salt = this.makeSalt();
  // encrypt password
  this.password = await this.encryptPassword(this.password);
  next();
});

// Methods
userSchema.methods = {
  authenticate: function (plainText) {
    // if encrypted typed password equal to saved hashed password =>
    // authenticated !
    return this.encryptPassword(plainText) === this.password;
  },
  encryptPassword: async function (password) {
    if (!password) return '';
    // // createHash
    const [error, result] = await tryAsyncAwait(hashPassword(password));
    return error ? '' : result;
  },
  makeSalt,
};

module.exports = mongoose.model('User', userSchema);
