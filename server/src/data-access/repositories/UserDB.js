const { User } = require('../models');

module.exports = function makeUserDb({ makeDb }) {
  makeDb();

  async function findAll({ params } = {}) {
    return await User.find({ params }).exec();
  }

  async function findOne(params) {
    return await User.findOne(params).exec();
  }

  async function findOneById(id) {
    return await User.findOne({ id }).exec();
  }

  async function findOneByEmail(email) {
    return await User.findOne({ email }).exec();
  }

  async function insert(data) {
    const newUser = new User(data);
    return await newUser.save();
  }

  async function update({ id, data }) {
    return await User.updateOne({ where: { id } }, { data }).exec();
  }

  async function remove(id) {
    const userToDelete = await User.findOne(id).exec();
    return await userToDelete.remove();
  }

  async function removeAll() {
    return await User.deleteMany().exec();
  }

  return Object.freeze({
    findAll,
    findOne,
    findOneById,
    findOneByEmail,
    insert,
    update,
    remove,
    removeAll,
  });
};
