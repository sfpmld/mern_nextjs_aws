const baseError = require('./baseError');
const { HTTP_ALREADY_EXIST_MESSAGE } = require('../constants/httpStatus');

module.exports = class extends baseError {
  constructor(message = HTTP_ALREADY_EXIST_MESSAGE) {
    super();
    this.message = message;
    this.trace = new Error(message);
  }
};
