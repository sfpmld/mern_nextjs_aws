const badRequestError = require('./badRequestError');
const baseError = require('./baseError');
const { HTTP_ALREADY_EXIST_MESSAGE } = require('../constants');

describe('badRequestError', () => {
  const errorMessage = 'error message!';
  let error;
  beforeAll(async () => {
    error = new badRequestError(errorMessage);
  });

  it('should extend baseError type', async () => {
    expect(error instanceof baseError).toBe(true);
  });
  it('should have message and trace properties', async () => {
    expect(error.message).toEqual(errorMessage);
    expect(error.trace).toEqual(new Error(errorMessage));
  });
});

describe('when message is not given', () => {
  const error = new badRequestError();

  it('should set error.message by default ${HTTP_ALREADY_EXIST_MESSAGE}', async () => {
    expect(error.message).toEqual(HTTP_ALREADY_EXIST_MESSAGE);
  });
});
