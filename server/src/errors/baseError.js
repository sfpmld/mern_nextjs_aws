module.exports = class baseError extends Error {
  constructor(message) {
    super();
    this.message = message;
    this.trace = new Error(message);
  }
}
