const baseError = require('./baseError');

describe('baseError', () => {
  const errorMessage = 'error message';
  let error;

  beforeAll(async () => {
    error = new baseError(errorMessage);
  });

  it('should be our base Error type that simply extends Error type', async () => {
    expect(error instanceof Error).toBe(true);
  });
  it('should have message property', async () => {
    expect(error.message).toBe(errorMessage);
  });
  it('should have a stack property that matchs to Error(message)', async () => {
    expect(error.trace).toEqual(new Error(errorMessage));
  });
});
