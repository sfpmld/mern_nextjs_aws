module.exports = class baseErrorWithData extends Error {
  constructor(message, data) {
    super();
    this.message = message;
    this.data = data;
    this.trace = new Error(message);
  }
};
