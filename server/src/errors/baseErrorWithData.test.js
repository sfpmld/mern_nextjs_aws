const baseErrorWithData = require('./baseErrorWithData');

describe('baseErrorWithData', () => {
  const errorMessage = 'error message';
  const data = {
    data: 'data',
  };
  let error;

  beforeAll(async () => {
    error = new baseErrorWithData(errorMessage, data);
  });

  it('should be our base Error type with Data, that simply extends Error type', async () => {
    expect(error instanceof Error).toBe(true);
  });
  it('should have a message property', async () => {
    expect(error.message).toBe(errorMessage);
  });
  it('should have a data property', async () => {
    expect(error.data).toBeTruthy();
  });
  it('should have a stack property that matchs Error(message)', async () => {
    expect(error.trace).toEqual(new Error(errorMessage));
  });
});
