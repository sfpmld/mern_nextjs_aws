const baseError = require("./baseError");

module.exports = class extends baseError{
  constructor(message) {
    super();
    this.message = message;
    this.trace = new Error(message);
  }
};
