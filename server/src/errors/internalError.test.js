const internalError = require('./internalError');
const baseError = require('./baseError');

describe('internalError', () => {
  const errorMessage = 'error message!';
  let error;
  beforeAll(async () => {
    error = new internalError(errorMessage);
  });

  it('should extend baseError type', async () => {
    expect(error instanceof baseError).toBe(true);
  });
  it('should have message and trace properties', async () => {
    expect(error.message).toEqual(errorMessage);
    expect(error.trace).toEqual(new Error(errorMessage));
  });
});
