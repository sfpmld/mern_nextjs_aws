const baseError = require('./baseError');

module.exports = class missingEnvVarError extends baseError {
  constructor(varName) {
    super();
    this.varName = varName;
    this.message = `env variable "${this.varName}" is missing`;
    this.trace = new Error(this.message);
  }
};
