const missingEnvVarError = require('./missingEnvVarError');
const simpleError = require('./baseError');

describe('missingEnvVarError', () => {
  const targetMessagePattern = /env variable.*is missing/;
  const varName = 'ENV_VAR';
  let error;
  beforeAll(async () => {
    error = new missingEnvVarError(varName);
  });

  it('should extend simpleError error type', async () => {
    expect(error instanceof simpleError).toBe(true);
  });
  it('should have message and trace properties', async () => {
    expect(error.message).toEqual(expect.stringMatching(targetMessagePattern));
    expect(error.trace).toEqual(new Error(error.message));
  });
  it('should have varName that is included in message property', async () => {
    expect(error.message).toEqual(expect.stringContaining(varName));
  });
});
