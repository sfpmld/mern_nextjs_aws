const baseErrorWithData = require('./baseErrorWithData');

module.exports = class validationError extends baseErrorWithData {
  constructor(message, data) {
    super();
    this.message = message;
    this.data = data;
    this.trace = new Error(message);
  }
};
