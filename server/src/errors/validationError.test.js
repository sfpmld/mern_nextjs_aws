const validationError = require('./validationError');
const baseErrorWithData = require('./baseErrorWithData');

describe('validationError', () => {
  const errorMessage = 'error message!';
  const data = {
    stuffs: 'stuff',
  };
  let error;
  beforeAll(async () => {
    error = new validationError(errorMessage, data);
  });

  it('should extend baseErrorWithData type', async () => {
    expect(error instanceof baseErrorWithData).toBe(true);
  });
  it('should have message and trace properties', async () => {
    expect(error.message).toEqual(errorMessage);
    expect(error.trace).toEqual(new Error(errorMessage));
  });
  it('should have data property', async () => {
    expect(error.data).toEqual(data);
  });
});
