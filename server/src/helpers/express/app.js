const express = require('express');

module.exports = class App {
  constructor() {
    this.instance;
  }
  getInstance() {
    return this.instance ? this.instance : express();
  }
};
