jest.mock('express', () => () => {
  return 'ok';
});
const app = require('./app');

describe('Express App Helper', () => {
  it('should return the same singleton instance of the express application at each getInstance call', async () => {
    const App = new app();

    expect(App.getInstance().toString()).toEqual(App.getInstance().toString());
  });
});
