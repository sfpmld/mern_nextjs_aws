const express = require('express');

module.exports = class Router {
  constructor() {
    this.instance;
  }
  getInstance() {
    return this.instance ? this.instance: express.Router()
  }
}
