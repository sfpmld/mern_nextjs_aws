jest.mock('express', () => ({
  Router: () => 'ok',
}));
const router = require('./router');

describe('Express Router Helper', () => {
  it('should return the same singleton express router at each getRouter call', async () => {
    const Router = new router();

    expect(Router.getInstance().toString()).toEqual(
      Router.getInstance().toString()
    );
  });
});
