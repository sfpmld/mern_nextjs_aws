const { tryAsyncAwait, tryPromise, getFromEnv } = require('./miscellaneous');
const { hashPassword } = require('./security');
const { makeSalt } = require('./security');
const { logger, consoleFormatter } = require('./logger');
const { templateRenderer, stringRenderer } = require('./templates');

module.exports = {
  tryAsyncAwait,
  tryPromise,
  getFromEnv,
  logger,
  consoleFormatter,
  hashPassword,
  makeSalt,
  templateRenderer,
  stringRenderer,
};
