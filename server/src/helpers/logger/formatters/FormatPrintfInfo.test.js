const { formatPrintfMessage } = require('../formatters');

describe('formatPrintfMessage', () => {
  let mockInfo = {
    level: 'info',
    timestamp: '2017-09-30T03:57:26.875Z',
    message: 'message',
  };

  it('should format printf message accordingly to pattern "[${level}] ${timestamp} ${message}"', async () => {
    const logMessage = formatPrintfMessage(mockInfo);
    const { timestamp, level, message } = mockInfo;

    expect(logMessage).toEqual(`[${level}] ${timestamp} ${message}`);
  });
  it('should format printf message accordingly to pattern "[${level}] ${timestamp} ${message} ${JSON.stringify(others)} if others given"', async () => {
    const others = {
      others: 'others',
      anotherOne: 'anotherOne',
    };
    mockInfo = { ...mockInfo, ...others };
    const logMessage = formatPrintfMessage(mockInfo);
    const { timestamp, level, message } = mockInfo;

    expect(logMessage).toEqual(
      `[${level}] ${timestamp} ${message} ${JSON.stringify(others)}`
    );
  });
});
