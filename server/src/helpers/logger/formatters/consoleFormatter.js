const { format } = require('winston');
const formatPrintfMessage = require('./formatPrintfMessage');

// logs with colorized output
const consoleFormatter = () => {
  const colorizer = format.colorize();
  return format.combine(
    format.timestamp(),
    format.printf(({ level, ...info }) => {
      // Write colorized logs.
      return colorizer.colorize(level, formatPrintfMessage(info));
    })
  );
};

module.exports = consoleFormatter;
