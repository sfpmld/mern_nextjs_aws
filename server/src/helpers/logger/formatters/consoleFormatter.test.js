jest.mock('../formatters/consoleFormatter');
const consoleFormatter = require('../formatters/consoleFormatter');

describe('Formatters', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });
  describe('consoleFormatter', () => {
    beforeEach(() => {
      const { logger } = require('../../logger');
      logger.info('testing consoleFormatter');
    });
    it('should call consoleFormatter to format message logged', async () => {
      expect(consoleFormatter).toHaveBeenCalled();
    });
  });
});
