const stringifyRestArgs = (...rest) => {
  const concatRestArgs = (acc, current) => {
    if (typeof current === 'string') return acc + current;
    if (typeof current === 'object') return acc + JSON.stringify(current);
  };
  return rest.reduce(concatRestArgs, ' ');
};

const formatPrintfMessage = ({ timestamp, level, message, ...rest }) => {
  return `[${level}] ${timestamp} ${message}${
    Object.keys(rest).length > 0 ? stringifyRestArgs(rest) : ''
  }`;
};

module.exports = formatPrintfMessage;
