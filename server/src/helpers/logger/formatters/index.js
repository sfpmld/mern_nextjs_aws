const consoleFormatter = require('./consoleFormatter');
const formatPrintfMessage = require('./formatPrintfMessage');

module.exports = {
  consoleFormatter,
  formatPrintfMessage,
};
