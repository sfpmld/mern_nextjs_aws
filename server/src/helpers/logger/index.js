const { logger, consoleFormatter } = require('./logger');

module.exports = {
  logger,
  consoleFormatter,
};
