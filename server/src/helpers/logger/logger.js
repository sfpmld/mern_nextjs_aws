const winston = require('winston');
const { transports } = require('winston');
const { consoleFormatter } = require('./formatters');

const {
  WINSTON_LOG_LEVEL = 'info',
  SERVICE_NAME = 'mern_nextjs_aws_back',
  SERVICE_VERSION = '1.0',
  ENVIRONMENT = 'dev',
} = process.env;

const logger = () => {
  return winston.createLogger({
    level: WINSTON_LOG_LEVEL,
    format: consoleFormatter(),
    defaultMeta: {
      service: SERVICE_NAME,
      version: SERVICE_VERSION,
      environment: ENVIRONMENT,
    },
    transports: [new transports.Console()],
    exceptionHandlers: [new transports.Console()],
  });
};

module.exports = { logger: logger(), consoleFormatter };
