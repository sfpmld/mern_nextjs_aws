const { missingEnvVarError } = require('../../errors');

const getFromEnv = varName => {
  const envVar = process.env[`${varName}`];
  if (!envVar) throw new missingEnvVarError(varName);
  return envVar;
};

module.exports = { getFromEnv };
