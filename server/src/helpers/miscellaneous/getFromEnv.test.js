const { getFromEnv } = require('./getFromEnv');
const { missingEnvVarError } = require('../../errors');

describe('getFromEnv utility function', () => {
  const varName = 'TEST_VAR';
  const otherVarName = 'OTHER_TEST_VAR';

  it('should get the env variable values from environment file or process', async () => {
    process.env[varName] = 'test';
    const env = getFromEnv(varName);

    expect(env).toEqual('test');
  });
  it('should throw missingEnvVarError if env var searched is missing', async () => {
    expect(() => getFromEnv(otherVarName)).toThrow(
      new missingEnvVarError(otherVarName)
    );
  });
});
