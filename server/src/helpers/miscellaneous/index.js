const { tryPromise, tryAsyncAwait } = require('./tryFn');
const { getFromEnv } = require('./getFromEnv');

module.exports = {
  tryPromise,
  tryAsyncAwait,
  getFromEnv,
};
