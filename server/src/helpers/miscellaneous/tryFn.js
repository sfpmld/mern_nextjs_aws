// async-await-to package approximation, to avoid too much dependencies
module.exports = {
  tryAsyncAwait: async expression => {
    let result;
    let error;
    try {
      result = await expression;
    } catch (err) {
      error = err;
    }
    return [error, result];
  },
  tryPromise: async expression => {
    try {
      return await expression;
    } catch (err) {
      console.log(err);
      throw err;
    }
  },
};
