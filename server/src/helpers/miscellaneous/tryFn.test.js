const { tryAsyncAwait, tryPromise } = require('./tryFn');

const errorMessage = 'Ooops!';
const fakeFunctionToResolve = () => new Promise((resolve, _) => resolve(true));
const fakeFunctionToReject = () =>
  new Promise((_, reject) => reject(new Error(errorMessage)));

describe('tryFn utilities function', () => {
  describe('tryAsyncAwait', () => {
    it('should return callback result with undefined error object: [undefined, result]', async () => {
      const [err, res] = await tryAsyncAwait(fakeFunctionToResolve());
      expect(res).toBeTruthy();
      expect(err).toBeFalsy();
    });
    it('should return an error object with undefined result: [error, undefined]', async () => {
      const [err, res] = await tryAsyncAwait(fakeFunctionToReject());
      expect(res).toBeFalsy();
      expect(err).toBeTruthy();
      expect(err.message).toEqual(errorMessage);
    });
  });

  describe('tryPromise', () => {
    it('should return callback result without throwing error', async () => {
      const res = await tryPromise(fakeFunctionToResolve());
      expect(res).toBeTruthy();
    });
    it('should throw error when catched', async () => {
      await expect(tryPromise(fakeFunctionToReject())).rejects.toThrow(
        errorMessage
      );
    });
  });
});
