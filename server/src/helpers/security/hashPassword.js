const crypto = require('crypto');

module.exports.hashPassword = password => {
  return crypto.createHash('sha1').update(password).digest('hex');
};
