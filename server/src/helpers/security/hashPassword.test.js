const { hashPassword } = require('./hashPassword');

jest.mock('crypto');
const crypto = require('crypto');
const mockUpdate = jest.fn();
const mockDigest = jest.fn();
mockUpdate.mockImplementation(() => ({
  digest: mockDigest,
}));
mockDigest.mockImplementation(() => 'digest');
crypto.createHash.mockReturnValue({
  update: mockUpdate,
});

describe('hashPassword utility function', () => {
  beforeEach(async () => {
    hashPassword(password);
  });
  const password = 'password';
  it('should hash password with sha1 algorithm', async () => {
    expect(crypto.createHash).toHaveBeenCalledWith('sha1');
  });
  it('should hash password with hex encoding', async () => {
    expect(mockDigest).toHaveBeenCalledWith('hex');
  });
});
