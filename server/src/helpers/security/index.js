const { hashPassword } = require('./hashPassword');
const { makeSalt } = require('./makeSalt');

module.exports = {
  hashPassword,
  makeSalt,
};
