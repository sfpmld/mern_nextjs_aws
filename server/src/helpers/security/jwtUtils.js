const jwt = require('jsonwebtoken');
const {
  JWT_ACCOUNT_ACTIVATION,
  JWT_ACCOUNT_ACTIVATION_EXPIRES,
} = require('../../constants');

class jwtUtils {
  constructor() {
    this.signJwt = jwt.sign;
  }

  sign(data, secret, expires) {
    return this.signJwt(data, secret, {
      expiresIn: expires,
    });
  }

  generateToken(data, secret, expires) {
    return this.sign(data, secret, expires);
  }

  generateAccountActivationToken(data) {
    return this.generateToken(
      data,
      JWT_ACCOUNT_ACTIVATION,
      JWT_ACCOUNT_ACTIVATION_EXPIRES
    );
  }
}

module.exports = jwtUtils;
