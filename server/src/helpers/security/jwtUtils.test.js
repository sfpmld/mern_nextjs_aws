const { makeFakeUser } = require('../../../__tests__/helpers');
const {
  JWT_ACCOUNT_ACTIVATION,
  JWT_ACCOUNT_ACTIVATION_EXPIRES,
} = require('../../constants');
jest.mock('../../helpers/logger');

const JWTUtils = require('./jwtUtils');

describe('JWT Utils', () => {
  const jwtUtils = new JWTUtils();

  let token;
  const user = makeFakeUser();
  const data = {
    name: user.name,
    email: user.email,
    password: user.password,
  };
  const secret = 'secret';
  const expires = '15d';

  describe('sign', () => {
    it('should be defined', async () => {
      expect(typeof jwtUtils.sign).toBe('function');
    });
    it('should return a token string', async () => {
      expect(typeof jwtUtils.sign(data, secret, expires)).toBe('string');
    });
  });

  describe('generateToken', () => {
    beforeAll(async () => {
      jwtUtils.sign = jest.fn(jwtUtils.sign);
      token = jwtUtils.generateToken(data, secret, expires);
    });

    it('should generate a token string from given arguments', async () => {
      expect(token).toBeTruthy();
      expect(typeof token).toBe('string');
    });
    it('should call sign method with given arguments', async () => {
      expect(jwtUtils.sign).toHaveBeenCalledWith(data, secret, expires);
    });
  });

  describe('generateAccountActivationToken', () => {
    beforeAll(async () => {
      jwtUtils.generateToken = jest.fn();
      token = jwtUtils.generateAccountActivationToken(data);
    });

    it('should call generateToken with data, "JWT_ACCOUNT_ACTIVATION" and "JWT_ACCOUNT_ACTIVATION_EXPIRES"', async () => {
      expect(jwtUtils.generateToken).toHaveBeenCalledWith(
        data,
        JWT_ACCOUNT_ACTIVATION,
        JWT_ACCOUNT_ACTIVATION_EXPIRES
      );
    });
  });
});
