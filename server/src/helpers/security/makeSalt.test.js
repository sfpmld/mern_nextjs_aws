const { makeSalt } = require('./makeSalt');

describe('makeSalt utility function', () => {
  it('should generate random salts string', async () => {
    const salt_1 = makeSalt();
    const salt_2 = makeSalt();

    expect(salt_1).toBeTruthy();
    expect(salt_2).toBeTruthy();
    expect(salt_1).not.toEqual(salt_2);
  });
});
