const { templateRenderer } = require('./templateRenderer');
const { stringRenderer } = require('./stringRenderer');

module.exports = {
  templateRenderer,
  stringRenderer,
};
