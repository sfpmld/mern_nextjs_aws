const { render } = require('mustache');

const stringRenderer = (stringTemplate, dataToInsert) => {
  return render(stringTemplate, dataToInsert);
};

module.exports = {
  stringRenderer,
};
