const { stringRenderer } = require('./stringRenderer');

const dataToInsert = { placeholder: 'string_included' };
const stringTemplate = 'a long string with {{ placeholder }} to fill in it';
describe('stringRenderer', () => {
  it(`should replace placeholder with "${dataToInsert.placeholder}" into the given string`, async () => {
    const formattedString = stringRenderer(stringTemplate, dataToInsert);

    expect(formattedString).toEqual(
      expect.stringContaining(dataToInsert.placeholder)
    );
  });
});
