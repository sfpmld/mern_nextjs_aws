const { render } = require('mustache');

const bufferToString = buffer => buffer.toString('utf8');

const templateRenderer = (buffer, data) => {
  return render(bufferToString(buffer), data);
};

module.exports = { templateRenderer };
