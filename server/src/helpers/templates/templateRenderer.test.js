const { templateRenderer } = require('./templateRenderer');

describe('templateRenderer', () => {
  const message = 'message';
  const buffer = `<html><body>${message}</body></html>`;
  let renderedMessage;
  beforeAll(async () => {
    renderedMessage = templateRenderer(buffer, message);
  });

  it('should render a string containing message', async () => {
    expect(renderedMessage).toEqual(expect.stringContaining(message));
  });
});
