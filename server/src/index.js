const { start } = require('./server');

const { SERVICE_NAME, PORT, ENV } = require('./constants');

// Starting Server
start({
  service: SERVICE_NAME,
  port: PORT,
  env: ENV,
});
