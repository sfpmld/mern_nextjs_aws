const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const helmet = require('helmet');

const commonMiddlewares = app => {
  // Common Middlewares
  app.use(bodyParser.json());
  app.use(morgan('dev'));
  app.use(helmet());

  // CORS Error Handling
  const corsOptions = {
    origin: process.env.CLIENT_URL,
    credentials: true,
    methods: "['OPTIONS','GET','HEAD','PUT','PATCH','POST','DELETE']",
    allowHeaders: "['Content-Type', 'Authorization']",
    optionsSuccessStatus: 200 /* graphql and some legacy browsers (IE11, various SmartTVs) choke on 204 */,
    preflightContinue: false,
  };

  app.use(cors(corsOptions));
};

module.exports = { commonMiddlewares };
