const { commonMiddlewares } = require('./commonMiddlewares');
const makeExpressCallback = require('./makeExpressCallback');
const { errorHandlingMiddleware } = require('./mainErrorHandler');

module.exports = {
  commonMiddlewares,
  errorHandlingMiddleware,
  makeExpressCallback,
};
