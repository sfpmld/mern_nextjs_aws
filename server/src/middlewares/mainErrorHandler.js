const {
  validationError,
  badRequestError,
  internalError,
} = require('../errors');
const { logger } = require('../helpers');

const {
  HTTP_UNPROCESSABLE_ENTITY,
  HTTP_BAD_REQUEST,
  HTTP_INTERNAL_ERROR,
  HTTP_INTERNAL_ERROR_MESSAGE,
} = require('../constants');
const missingEnvVarError = require('../errors/missingEnvVarError');

const errorLogger = (error, _req, _res, next) => {
  if (error.message) {
    logger.error(error.message);
  }
  if (error.trace) {
    logger.warn(error.trace);
  }
  next(error);
};

const validationErrorHandler = (error, _req, _res, next) => {
  if (error instanceof validationError) {
    error.status = HTTP_UNPROCESSABLE_ENTITY;
  }
  next(error);
};

const missingEnvVarErrorHandler = (error, _req, _res, next) => {
  if (error instanceof missingEnvVarError) {
    error.status = HTTP_INTERNAL_ERROR;
  }
  next(error);
};

const badRequestErrorHandler = (error, _req, _res, next) => {
  if (error instanceof badRequestError) {
    error.status = HTTP_BAD_REQUEST;
  }
  next(error);
};

const internalErrorHandler = (error, _req, _res, next) => {
  if (error instanceof internalError) {
    error.status = HTTP_INTERNAL_ERROR;
  }
  next(error);
};

const genericErrorHandler = (error, _req, res, next) => {
  const statusCode = error.status || HTTP_INTERNAL_ERROR;
  const message = !error.status ? HTTP_INTERNAL_ERROR_MESSAGE : error.message;

  res.status(statusCode).json({
    data: {},
    error: {
      message,
      status: statusCode,
    },
  });
  next();
};

module.exports = {
  errorLogger,
  validationErrorHandler,
  missingEnvVarErrorHandler,
  badRequestErrorHandler,
  internalErrorHandler,
  genericErrorHandler,
  errorHandlingMiddleware: app => {
    app.use([
      errorLogger,
      validationErrorHandler,
      missingEnvVarErrorHandler,
      badRequestErrorHandler,
      internalErrorHandler,
      genericErrorHandler,
    ]);
  },
};
