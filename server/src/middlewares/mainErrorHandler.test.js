const {
  // these import are treated by an eval function inside local helpers
  errorLogger,
  // eslint-disable-next-line
  badRequestErrorHandler,
  // eslint-disable-next-line
  missingEnvVarErrorHandler,
  // eslint-disable-next-line
  validationErrorHandler,
  // eslint-disable-next-line
  internalErrorHandler,
  genericErrorHandler,
} = require('../middlewares/mainErrorHandler');
const {
  // eslint-disable-next-line
  validationError,
  // eslint-disable-next-line
  missingEnvVarError,
  // eslint-disable-next-line
  badRequestError,
  // eslint-disable-next-line
  internalError,
} = require('../errors');

jest.mock('../helpers/logger');
const { logger } = require('../helpers/logger');

const {
  HTTP_UNPROCESSABLE_ENTITY,
  HTTP_INTERNAL_ERROR,
  HTTP_INTERNAL_ERROR_MESSAGE,
  HTTP_BAD_REQUEST,
} = require('../constants');

const mockResponseStatus = jest.fn();
const mockResponseJson = jest.fn();
const mockResponse = {
  status: s => {
    mockResponseStatus(s);
    return mockResponse;
  },
  json: m => mockResponseJson(m),
};
const mockNext = jest.fn();
logger.error = jest.fn();
logger.warn = jest.fn();

const runErrorHandler = (type, error) => {
  const errorHandler = eval(type);
  return errorHandler(error, {}, mockResponse, mockNext);
};
const throwError = (type, message, data) => {
  const error = eval(type);
  return new error(message, data);
};

describe('mainErrorHandler', () => {
  beforeEach(async () => {
    mockResponseStatus.mockClear();
    mockResponseJson.mockClear();
    mockNext.mockClear();
  });
  describe('errorLogger', () => {
    const error = new Error();
    error.message = 'error-message';
    error.trace = 'error-trace';

    beforeEach(async () => {
      errorLogger(error, {}, {}, mockNext);
    });

    it('should log your error message', async () => {
      expect(logger.error).toHaveBeenCalled();
    });
    it('should log your error stack trace', async () => {
      expect(logger.warn).toHaveBeenCalled();
    });
    it('should pass the error object to the next function', async () => {
      expect(mockNext).toHaveBeenCalledWith(error);
    });
  });

  describe('Custom Error Handler:', () => {
    const errorData = { errorData: 'error-data' };
    const handlers = [
      [
        'validationErrorHandler',
        'validationError',
        'validation-error',
        HTTP_UNPROCESSABLE_ENTITY,
        errorData,
      ],
      [
        'missingEnvVarErrorHandler',
        'missingEnvVarError',
        'missing-envVar-error',
        HTTP_INTERNAL_ERROR,
        undefined,
      ],
      [
        'badRequestErrorHandler',
        'badRequestError',
        'bad-request-error',
        HTTP_BAD_REQUEST,
        undefined,
      ],
      [
        'internalErrorHandler',
        'internalError',
        'internal-error',
        HTTP_INTERNAL_ERROR,
        undefined,
      ],
    ];

    describe.each(handlers)(
      '%s',
      (errorHandlerType, errorType, errorMessage, errorStatusCode, data) => {
        let error;
        beforeAll(async () => {
          error = throwError(errorType, errorMessage, {
            errorData: data,
          });
        });
        beforeEach(async () => {
          runErrorHandler(errorHandlerType, error);
        });

        it(`should have set the status to ${errorStatusCode} for the error object before to be passed trough the next function`, async () => {
          expect(error.status).toBe(errorStatusCode);
          expect(mockNext).toHaveBeenCalledWith(error);
        });
      }
    );
  });

  describe('genericErrorHandler', () => {
    describe('given an unknown error thrown', () => {
      let error;
      beforeAll(async () => {
        error = new Error('UNKNOWN ERROR');
      });
      beforeEach(async () => {
        genericErrorHandler(error, {}, mockResponse, mockNext);
      });
      it(`should send ${HTTP_INTERNAL_ERROR} if error status code is unknown`, async () => {
        expect(mockResponseStatus).toHaveBeenCalledWith(HTTP_INTERNAL_ERROR);
      });
      it(`should send a json object with message "${HTTP_INTERNAL_ERROR_MESSAGE}" and status ${HTTP_INTERNAL_ERROR}`, async () => {
        expect(mockResponseJson).toHaveBeenCalledWith({
          data: {},
          error: {
            message: HTTP_INTERNAL_ERROR_MESSAGE,
            status: HTTP_INTERNAL_ERROR,
          },
        });
      });
      it(`should call next function with 0 argument`, async () => {
        expect(mockNext).not.toHaveBeenCalledWith(error);
      });
    });
    describe('given a well known error like:', () => {
      let error;

      const errorData = { errorData: 'error-data' };
      const errorList = [
        [
          'validationError',
          'validation-error',
          HTTP_UNPROCESSABLE_ENTITY,
          errorData,
        ],
        ['missingEnvVarError', 'missingEnvVar', HTTP_INTERNAL_ERROR, undefined],
        ['internalError', 'internal-error', HTTP_INTERNAL_ERROR, undefined],
        ['badRequestError', 'bad-request-error', HTTP_BAD_REQUEST, undefined],
      ];

      describe.each(errorList)(
        '%s',
        (errorType, errorMessage, errorStatusCode, data) => {
          beforeAll(async () => {
            error = throwError(errorType, errorMessage, {
              errorData: data,
            });
            error.status = errorStatusCode;
          });
          beforeEach(async () => {
            genericErrorHandler(error, {}, mockResponse, mockNext);
          });

          it(`should return ${errorStatusCode}`, async () => {
            expect(mockResponseStatus).toHaveBeenCalledWith(errorStatusCode);
          });
          it(`should send a json object with message containing "${errorMessage}" and status "${errorStatusCode}"`, async () => {
            if (errorType === 'missingEnvVarError')
              errorMessage = `env variable "${errorMessage}" is missing`;

            expect(mockResponseJson).toHaveBeenCalledWith({
              data: {},
              error: {
                message: errorMessage,
                status: errorStatusCode,
              },
            });
          });
          it(`should call next function with 0 argument`, async () => {
            expect(mockNext).not.toHaveBeenCalledWith(error);
          });
        }
      );
    });
  });
});
