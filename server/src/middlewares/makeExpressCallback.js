function makeExpressCallback(middleware) {
  return async (req, res, next) => {
    const httpRequest = {
      path: req.path,
      originalUrl: req.originalUrl,
      method: req.method,
      params: req.params,
      query: req.query,
      body: req.body,
      req,
      res,
      next,
      headers: {
        'Content-Type': req.get('Content-Type'),
        Referer: req.get('referer'),
        'User-Agent': req.get('User-Agent'),
      },
    };

    try {
      const httpResponse = await middleware(httpRequest);
      if (httpResponse.headers) {
        res.set(httpResponse.headers);
      } else {
        // default to json
        res.set({ 'Content-Type': 'application/json' });
      }
      // sending response to client
      return res.status(httpResponse.statusCode).json(httpResponse.body);
    } catch (err) {
      return next(err);
    }
  };
}

module.exports = makeExpressCallback;
