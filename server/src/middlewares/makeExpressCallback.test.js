const { callMiddleware } = require('../../__tests__/helpers/callMiddleware');
const { HTTP_OK } = require('../constants');
jest.mock('../helpers/logger');

describe('makeExpressCallback middleware', () => {
  const mockRequestGet = jest.fn(type => {
    let header;
    switch (type) {
      case 'Content-Type':
        header = 'content-type';
        break;
      case 'referer':
        header = 'referer';
        break;
      case 'User-Agent':
        header = 'user-agent';
        break;
    }
    return header;
  });
  const mockRequest = jest.fn();
  mockRequest.get = mockRequestGet;
  const mockResponse = jest.fn();
  mockResponse.set = jest.fn();
  mockResponse.status = jest.fn(() => mockResponse);
  mockResponse.json = jest.fn(() => mockResponse);
  const mockNext = jest.fn();
  const httpRequest = {
    path: 'path',
    originalUrl: 'originalUrl',
    method: 'http method',
    params: 'params',
    query: 'query',
    body: 'body',
    req: mockRequest,
    res: mockResponse,
    next: mockNext,
    headers: {
      'Content-Type': 'content-type',
      Referer: 'referer',
      'User-Agent': 'user-agent',
    },
  };
  Object.assign(mockRequest, httpRequest);
  const mockMiddleware = jest.fn();

  beforeEach(async () => {
    mockRequest.mockClear();
    mockRequestGet.mockClear();
    mockResponse.mockClear();
    mockResponse.set.mockClear();
    mockNext.mockClear();
    mockMiddleware.mockClear();
  });
  it('should pass your http request object to your middleware', async () => {
    mockMiddleware.mockResolvedValue({});
    await callMiddleware(mockMiddleware, mockRequest, mockResponse, mockNext);

    expect(mockMiddleware).toHaveBeenCalledWith(httpRequest);
  });
  it('should set "application/json" Content-Type default headers if not already set inside controller', async () => {
    mockMiddleware.mockResolvedValue({
      statusCode: HTTP_OK,
      body: 'body',
    });
    await callMiddleware(mockMiddleware, mockRequest, mockResponse, mockNext);

    expect(mockResponse.set).toHaveBeenCalledWith({
      'Content-Type': 'application/json',
    });
  });
  it('should set given headers from inside controller', async () => {
    const headers = {
      'Content-Type': 'header',
    };
    mockMiddleware.mockResolvedValue({
      headers,
    });
    await callMiddleware(mockMiddleware, mockRequest, mockResponse, mockNext);

    expect(mockResponse.set).toHaveBeenCalledWith({
      ...headers,
    });
  });
  it('should next error if an error is thrown', async () => {
    const error = new Error('error!');
    mockMiddleware.mockRejectedValue(error);
    mockNext.mockImplementationOnce(() => jest.fn());
    await callMiddleware(mockMiddleware, mockRequest, mockResponse, mockNext);

    expect(mockNext).toHaveBeenCalledWith(error);
  });
  it(`should send json response with status ${HTTP_OK} and body object`, async () => {
    const httpResponse = {
      statusCode: HTTP_OK,
      body: 'body',
    };
    mockMiddleware.mockResolvedValue(httpResponse);
    await callMiddleware(mockMiddleware, mockRequest, mockResponse, mockNext);
    expect(mockResponse.status).toHaveBeenCalledWith(httpRequest.status);
    expect(mockResponse.json).toHaveBeenCalledWith(httpRequest.body);
  });
});
