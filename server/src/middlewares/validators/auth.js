const { check } = require('express-validator');

module.exports = {
  userRegisterValidator: [
    check('name', 'Name is required').not().isEmpty(),
    check('email', 'Must be a valid email').isEmail(),
    check(
      'password',
      'Password must container more than 6 characters'
    ).isLength({
      min: 6,
    }),
  ],
};
