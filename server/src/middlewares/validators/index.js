const { validationResult } = require('express-validator');
const authValidator = require('./auth');
const { validationError } = require('../../errors');

module.exports = {
  ...authValidator,
  runValidation: (req, _res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const data = errors.array()[0];
      const message =
        'Validation failed, entered data is incorrect: ' +
        errors.array()[0].msg;
      return next(new validationError(message, data));
    }
    next();
  },
};
