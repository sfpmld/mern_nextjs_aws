const {
  HTTP_OK,
  HTTP_ALREADY_EXIST_MESSAGE,
  EMAIL_SENT,
  EMAIL_NOT_SENT,
  JSON_EMAIL_SENT_TEMPLATE,
} = require('../../constants');
const { badRequestError } = require('../../errors');
const JWTUtils = require('../../helpers/security/jwtUtils');
const { logger, tryAsyncAwait, stringRenderer } = require('../../helpers');
const {
  sendEmail,
  createAccountActivationEmailConfig,
} = require('../../services');

const makeRegister = ({ UserDb }) => {
  const jwtUtils = new JWTUtils();
  const findUserByEmail = async email => {
    return await UserDb.findOneByEmail(email);
  };
  const throwsIfUserAlreadyExists = async email => {
    const isAlreadyRegistered = await findUserByEmail(email);

    if (isAlreadyRegistered) {
      throw new badRequestError(HTTP_ALREADY_EXIST_MESSAGE);
    }
  };
  const generateToken = data => jwtUtils.generateAccountActivationToken(data);
  const sendAccountActivationEmail = async (email, data) => {
    const emailConfig = await createAccountActivationEmailConfig(email, data);

    return await tryAsyncAwait(sendEmail.send(emailConfig));
  };
  const logEmailSentWhenSuccess = res => res && logger.info(EMAIL_SENT);
  const logEmailNotSentWhenFailure = err => err && logger.warn(EMAIL_NOT_SENT);
  const bodySuccessMessage = email => {
    const successMessage = JSON_EMAIL_SENT_TEMPLATE;
    const data = {
      email,
    };
    return stringRenderer(successMessage, data);
  };

  return async function register({ body }) {
    const { name, email } = body;
    const userData = {
      name,
      email,
    };

    await throwsIfUserAlreadyExists(email);

    const emailData = {
      token: generateToken(userData),
    };

    const [err, res] = await sendAccountActivationEmail(email, emailData);

    logEmailSentWhenSuccess(res);
    logEmailNotSentWhenFailure(err);

    return {
      body: {
        message: bodySuccessMessage(email),
      },
      statusCode: HTTP_OK,
    };
  };
};

module.exports = {
  makeRegister,
};
