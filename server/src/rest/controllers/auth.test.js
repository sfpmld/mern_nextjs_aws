const { makeRegister } = require('./auth');
const { makeFakeUser } = require('../../../__tests__/helpers');
const {
  HTTP_ALREADY_EXIST_MESSAGE,
  EMAIL_SENT,
  EMAIL_NOT_SENT,
} = require('../../constants');
const { badRequestError } = require('../../errors');
const mockGenerateAccountActivationToken = jest.fn(() => 'token');
jest.mock('../../helpers/security/jwtUtils', () => {
  return jest.fn().mockImplementation(() => {
    return {
      generateAccountActivationToken: mockGenerateAccountActivationToken,
    };
  });
});

jest.mock('../../services/email', () => ({
  sendEmail: {
    send: jest.fn(() => 'email sent'),
  },
  createAccountActivationEmailConfig: jest.fn(),
}));

const { sendEmail } = require('../../services/email');
const { createAccountActivationEmailConfig } = require('../../services');
const accountActivationMessage = 'configs';
createAccountActivationEmailConfig.mockImplementation(
  () => accountActivationMessage
);
jest.mock('../../helpers/logger', () => ({
  logger: {
    info: jest.fn(),
    warn: jest.fn(),
  },
}));
const { logger } = require('../../helpers/logger');

describe('Auth Controller', () => {
  describe('register api', () => {
    const user = {
      ...makeFakeUser(),
    };
    const name = user.name;
    const email = user.email;
    const data = {
      name,
      email,
    };
    const body = {
      ...data,
    };
    const UserDb = {
      findOneByEmail: jest.fn(),
    };
    let register = makeRegister({ UserDb });

    let response;
    beforeAll(async () => {
      response = await register({ body });
    });

    it(`Should verify if user is present in database, calling UserDb.findUserByEmail with ${email} `, async () => {
      expect(UserDb.findOneByEmail).toHaveBeenCalledTimes(1);
      expect(UserDb.findOneByEmail).toHaveBeenCalledWith(email);
    });

    describe('When user is not present in database', () => {
      it('should generate a token to send back to client, calling generateAccountActivationToken helper', async () => {
        expect(mockGenerateAccountActivationToken).toHaveBeenCalledTimes(1);
      });
      it('should generate this token with {name, email, password}', async () => {
        expect(mockGenerateAccountActivationToken).toHaveBeenCalledWith(data);
      });
      it('should generate email configuration message object for account activation', async () => {
        expect(createAccountActivationEmailConfig).toHaveBeenCalledTimes(1);
      });
      it('should send an email with configs params', async () => {
        expect(sendEmail.send).toHaveBeenCalledTimes(1);
        expect(sendEmail.send).toHaveBeenCalledWith(accountActivationMessage);
      });

      describe('When email is sent Successfully:', () => {
        it(`should log success message "${EMAIL_SENT}"`, async () => {
          expect(logger.info).toHaveBeenCalledWith(EMAIL_SENT);
        });
      });
      describe('When email is not sent Successfully:', () => {
        beforeAll(async () => {
          sendEmail.send.mockReturnValue(
            new Promise((_resolve, reject) => {
              return reject(new Error('some email service error!'));
            })
          );
          register({ body });
        });
        it(`should log failure message "${EMAIL_NOT_SENT}"`, async () => {
          expect(logger.warn).toHaveBeenCalledWith(EMAIL_NOT_SENT);
        });
      });

      it(`should return a response object with message field`, async () => {
        expect(response.body).toEqual(
          expect.objectContaining({
            message: expect.stringContaining(email),
          })
        );
      });
    });

    describe('When user is present in database', () => {
      const UserDb = {
        findOneByEmail: jest.fn(() => user),
      };
      let register = makeRegister({ UserDb });
      let result;
      beforeAll(async () => {
        result = register({ body });
      });

      it(`should throw a BAD_REQUEST error with ${HTTP_ALREADY_EXIST_MESSAGE} message`, async () => {
        UserDb.findOneByEmail.mockReturnValue(user);
        return await expect(result).rejects.toThrow(
          new badRequestError(HTTP_ALREADY_EXIST_MESSAGE)
        );
      });
    });
  });
});
