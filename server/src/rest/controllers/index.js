const { makeRegister } = require('./auth');
const { UserDb } = require('../../data-access');

const register = makeRegister({ UserDb });

module.exports = {
  auth: {
    register,
  },
};
