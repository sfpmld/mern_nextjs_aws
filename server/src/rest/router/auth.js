const { Router } = require('../../helpers/express');
const { makeExpressCallback } = require('../../middlewares');
const router = Router.getInstance();

const { register } = require('../controllers').auth;
const {
  userRegisterValidator,
  runValidation,
} = require('../../middlewares/validators');

router.post(
  '/register',
  userRegisterValidator,
  runValidation,
  makeExpressCallback(register)
);

module.exports = router;
