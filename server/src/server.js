const { App } = require('./helpers/express');
const { dbConnect, dbDisconnect } = require('./data-access');
const { authRoutes } = require('./rest');
const { commonMiddlewares } = require('./middlewares');
const { errorHandlingMiddleware } = require('./middlewares');

const createApp = ({ App }) => {
  const app = App.getInstance();
  commonMiddlewares(app);
  // // routes
  app.use('/api', authRoutes);
  // main error handler
  errorHandlingMiddleware(app);

  return app;
};

const logServerStateSuccess = (service, port, env) => {
  console.log(`Server "${service}" started`);
  console.log(`Environment: ${env}, Listening on port: ${port}`);
};
const logServerStateClosed = service => {
  console.log(`Server "${service}" closed.`);
};
const logServerStateFailed = err => {
  console.log(`Server start failed: ${err}`);
};

const start = async ({
  service,
  port,
  env,
  logSuccess = logServerStateSuccess,
  logFailed = logServerStateFailed,
}) => {
  const app = createApp({ App });
  const httpServer = new Promise((resolve, reject) => {
    try {
      resolve(
        app.listen(port, () => {
          resolve(this);
        })
      );
    } catch (err) {
      reject(err);
    }
  });
  return httpServer
    .then(async httpServer => {
      logSuccess(service, port, env);
      await dbConnect();
      return httpServer;
    })
    .catch(err => logFailed(err));
};

const stop = ({ server, service, logClosed = logServerStateClosed }) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(server.close());
      logClosed(service);
      dbDisconnect();
    } catch (err) {
      reject(err);
    }
  });
};

module.exports = {
  createApp,
  start,
  stop,
};
