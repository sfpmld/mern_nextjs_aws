const { SERVICE_NAME, PORT, ENV } = require('./constants');

const mockUse = jest.fn();
const mockListen = jest.fn();
const mockApp = () => ({
  use: mockUse,
  listen: mockListen,
});
const mockRouter = jest.fn();
Object.assign(mockRouter, {
  get: jest.fn(),
  post: jest.fn(),
  delete: jest.fn(),
});
Object.assign(mockApp, {
  Router: () => mockRouter,
});
jest.doMock('express', () => mockApp);

const server = require('./server');
const serverConfig = {
  service: SERVICE_NAME,
  port: PORT,
  env: ENV,
};
const mockLogServerStateSuccess = jest.fn();
const mockLogServerStateFailed = jest.fn();
const mockLogServerStateClosed = jest.fn();
const serverConfigWithStubLogger = {
  service: SERVICE_NAME,
  port: PORT,
  env: ENV,
  logSuccess: mockLogServerStateSuccess,
  logFailed: mockLogServerStateFailed,
};
const initServerSuccess = async config => {
  mockListen.mockImplementationOnce((_port, cb) => {
    cb();
  });
  return server.start(config);
};
const initServerFailed = async config => {
  mockListen.mockImplementationOnce((_port, _cb) => {
    throw new Error('failed!');
  });
  return server.start(config);
};

const mockGetInstance = jest.fn();
mockGetInstance.mockImplementation(() => ({
  use: () => jest.fn(),
}));
jest.doMock('./helpers/express/app', () => ({
  getInstance: mockGetInstance,
}));
const App = require('./helpers/express/app');

const { commonMiddlewares } = require('./middlewares/commonMiddlewares');
jest.mock('./middlewares/commonMiddlewares', () => ({
  commonMiddlewares: jest.fn(),
}));

const { errorHandlingMiddleware } = require('./middlewares/mainErrorHandler');
jest.mock('./middlewares/mainErrorHandler', () => ({
  errorHandlingMiddleware: jest.fn(),
}));

const { dbConnect, dbDisconnect } = require('./data-access');
jest.mock('./data-access', () => ({
  dbConnect: jest.fn(() => 'MongoDB Started!'),
  dbDisconnect: jest.fn(() => 'MongoDB Stoped!'),
}));

describe('Http Server', () => {
  afterEach(async () => {
    jest.resetAllMocks();
  });
  describe('createApp', () => {
    it('should create and configure Express Application', async () => {
      server.createApp({ App });

      expect(App.getInstance).toHaveBeenCalled();
      expect(commonMiddlewares).toHaveBeenCalled();
      expect(errorHandlingMiddleware).toHaveBeenCalled();
    });
  });
  describe('start', () => {
    it('should start the http server', async () => {
      const httpServer = await initServerSuccess(serverConfig);

      expect(mockListen).toHaveBeenCalled();
      expect(httpServer).toBeTruthy();
      expect(typeof httpServer).toBe('object');
    });
    it('should start DB connection when http server successfully started', async () => {
      await initServerSuccess(serverConfig);

      expect(dbConnect).toHaveBeenCalledTimes(1);
    });
  });

  describe('stop', () => {
    let mockServer = {};
    mockServer.close = jest.fn();
    beforeAll(async () => {});
    afterEach(async () => {
      jest.resetAllMocks();
    });
    it('should stop the http server', async () => {
      await server.stop({
        server: mockServer,
        service: 'service',
      });

      expect(mockServer.close).toHaveBeenCalled();
    });
    it('should stop DB Connection when http server closed', async () => {
      await server.stop({
        server: mockServer,
        service: 'service',
      });

      expect(dbDisconnect).toHaveBeenCalledTimes(1);
    });
  });

  describe('logServerStateSuccess', () => {
    it('should call logServerStateSuccess at start when no error is thrown', async () => {
      await initServerSuccess(serverConfigWithStubLogger);

      expect(mockLogServerStateSuccess).toHaveBeenCalled();
    });
    it(`should log service name ${SERVICE_NAME}, port ${PORT} and env ${ENV} vars in console.log message when starting`, async () => {
      const spyConsoleLog = jest.spyOn(global.console, 'log');
      await initServerSuccess(serverConfig);

      expect(spyConsoleLog.mock.calls[0][0]).toEqual(
        expect.stringContaining(SERVICE_NAME)
      );
      expect(spyConsoleLog.mock.calls[1][0]).toEqual(
        expect.stringContaining(PORT)
      );
      expect(spyConsoleLog.mock.calls[1][0]).toEqual(
        expect.stringContaining(ENV)
      );
    });
  });

  describe('logServerStateFailed', () => {
    it('should call logServerStateFailed at start when no error is thrown', async () => {
      await initServerFailed(serverConfigWithStubLogger);

      expect(mockLogServerStateFailed).toHaveBeenCalled();
    });
    it(`should log port ${PORT} and env ${ENV} vars in console.log message when starting`, async () => {
      const spyConsoleLog = jest.spyOn(global.console, 'log');
      await initServerFailed(serverConfig);

      expect(spyConsoleLog.mock.calls[0][0]).toEqual(
        expect.stringContaining('Error')
      );
    });
  });

  describe('logServerStateStop', () => {
    it('should call logServerStateFailed at start when no error is thrown', async () => {
      const mockServer = {};
      mockServer.close = jest.fn();
      await server.stop({
        server: mockServer,
        service: 'service',
        logClosed: mockLogServerStateClosed,
      });

      expect(mockLogServerStateClosed).toHaveBeenCalled();
    });
    it('should log logServerStateStop when closing server', async () => {
      const spyConsoleLog = jest.spyOn(global.console, 'log');
      const mockServer = {};
      mockServer.close = jest.fn();
      await server.stop({
        server: mockServer,
        service: 'service',
      });

      expect(spyConsoleLog.mock.calls[0][0]).toEqual(
        expect.stringMatching('closed')
      );
    });
  });
});
