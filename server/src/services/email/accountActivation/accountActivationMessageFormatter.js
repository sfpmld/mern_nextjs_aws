const { messageFormatter } = require('../common/messageFormatter');
const { templateRenderer } = require('../../../helpers/templates');

const accountActivationMessageFormatter = async data => {
  const formatter = new messageFormatter(
    'accountActivation.html',
    data,
    templateRenderer
  );
  await formatter.getTemplate();
  return await formatter.render();
};

module.exports = {
  accountActivationMessageFormatter,
};
