const fs = require('fs').promises;
const {
  accountActivationMessageFormatter,
} = require('./accountActivationMessageFormatter');
jest.mock('../../../helpers/logger');

describe('accountActivationMessageFormatter', () => {
  const message = 'message';
  const data = {
    message,
  };
  const mockFSRead = jest.spyOn(fs, 'readFile');
  const templateContent = 'template content with {{ message }} anchors';
  mockFSRead.mockResolvedValue(templateContent);

  let activationMessage;
  beforeAll(async () => {
    activationMessage = await accountActivationMessageFormatter(data);
  });
  it('should return a string', async () => {
    expect(typeof activationMessage).toBe('string');
  });

  it(`should return a string containing "${Object.values(data)}"`, async () => {
    Object.values(data).forEach(d =>
      expect(d).toEqual(expect.stringContaining(message))
    );
  });
});
