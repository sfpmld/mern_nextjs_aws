const { createEmailConfig } = require('../common/createEmailConfig');
const { accountActivationSubject } = require('../../../constants');
const {
  accountActivationMessageFormatter,
} = require('./accountActivationMessageFormatter');

const createAccountActivationEmailConfig = (email, data) => {
  return createEmailConfig({
    email,
    subject: accountActivationSubject,
    data,
    messageFormatter: accountActivationMessageFormatter,
  });
};

module.exports = {
  createAccountActivationEmailConfig,
};
