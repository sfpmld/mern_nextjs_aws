const {
  createAccountActivationEmailConfig,
} = require('./createAccountActivationEmailConfig');
const { createEmailConfig } = require('../common/createEmailConfig');
jest.mock('../common/createEmailConfig');
const originalModule = jest.requireActual('../common/createEmailConfig');
const {
  accountActivationMessageFormatter,
} = require('./accountActivationMessageFormatter');
const data = {
  message: 'message',
  otherField: 'other field',
};
jest.mock('./accountActivationMessageFormatter');
accountActivationMessageFormatter.mockImplementation(
  () => `string with ${data.message} and ${data.otherField}`
);

const {
  accountActivationSubject,
  emailCharset,
  EMAIL_FROM,
  EMAIL_TO,
} = require('../../../constants');

describe('createAccountActivationEmailConfig Service', () => {
  const email = 'email';
  const expectedEmailConfig = {
    Destination: {
      ToAddress: [email],
    },
    Message: {
      Body: {
        Html: {
          Charset: emailCharset,
          Data: 'string containing data values passed',
        },
      },
      Subject: {
        Charset: emailCharset,
        Data: accountActivationSubject,
      },
    },
    ReplyToAddresses: [EMAIL_TO],
    Source: EMAIL_FROM,
  };

  beforeAll(async () => {
    createAccountActivationEmailConfig(email, data);
  });

  it('should call createEmailConfig', async () => {
    expect(createEmailConfig).toHaveBeenCalledTimes(1);
  });

  it('with email, accountActivationSubject, accountActivationMessage, accountActivationMessageFormatter', async () => {
    expect(createEmailConfig).toHaveBeenCalledWith({
      email,
      subject: accountActivationSubject,
      data,
      messageFormatter: accountActivationMessageFormatter,
    });
  });

  it('should return an object corresponding to email configuration datastructure', async () => {
    createEmailConfig.mockImplementation(originalModule.createEmailConfig);
    const emailMessage = await createAccountActivationEmailConfig(email, data);

    expect(emailMessage.Destination).toEqual(expectedEmailConfig.Destination);
    expect(emailMessage.Message.Body.Html.Charset).toEqual(
      expectedEmailConfig.Message.Body.Html.Charset
    );
    Object.values(data).forEach(d =>
      expect(emailMessage.Message.Body.Html.Data).toEqual(
        expect.stringContaining(d)
      )
    );
    expect(emailMessage.Message.Subject).toEqual(
      expectedEmailConfig.Message.Subject
    );
    expect(emailMessage.ReplyToAddresses).toEqual(
      expectedEmailConfig.ReplyToAddresses
    );
    expect(emailMessage.Source).toEqual(expectedEmailConfig.Source);
  });
});
