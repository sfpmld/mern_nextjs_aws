const {
  accountActivationMessageFormatter,
} = require('./accountActivationMessageFormatter');
const {
  createAccountActivationEmailConfig,
} = require('./createAccountActivationEmailConfig');

module.exports = {
  accountActivationMessageFormatter,
  createAccountActivationEmailConfig,
};
