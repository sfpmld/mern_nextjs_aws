const AWS = require('aws-sdk');
const { getFromEnv } = require('../../../helpers');

class AWSClient {
  static connect() {
    AWS.config.update({
      aws_access_key_id: getFromEnv('AWS_ACCESS_KEY_ID'),
      aws_secret_access_key: getFromEnv('AWS_SECRET_ACCESS_KEY'),
      aws_region: getFromEnv('AWS_REGION'),
    });

    return AWS;
  }
}

module.exports = { AWSClient };
