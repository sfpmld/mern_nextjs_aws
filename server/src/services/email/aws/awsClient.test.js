const { AWSClient } = require('./awsClient');
const { missingEnvVarError } = require('../../../errors');
jest.mock('../../../helpers/logger');

describe('AWSClient', () => {
  beforeAll(async () => {
    removeAWSCredentialsFromEnv(['AWS_ACCESS_KEY_ID', 'AWS_SECRET_ACCESS_KEY']);
  });

  describe('should throw missingEnvVarError when', () => {
    it.each([
      [
        'AWS_ACCESS_KEY_ID mandatory environment variable is missing',
        ['AWS_SECRET_ACCESS_KEY', 'AWS_REGION'],
        ['AWS_ACCESS_KEY_ID'],
      ],
      [
        'AWS_SECRET_ACCESS_KEY mandatory environment variable is missing',
        ['AWS_ACCESS_KEY_ID'],
        ['AWS_SECRET_ACCESS_KEY', 'AWS_REGION'],
      ],
      [
        'AWS_REGION mandatory environment variable is missing',
        ['AWS_ACCESS_KEY_ID', 'AWS_SECRET_ACCESS_KEY'],
        ['AWS_REGION'],
      ],
    ])('%s', (_name, presentEnvVars, missingEnvVars) => {
      givenAWSCredentialsRemovedFromEnv(missingEnvVars);
      givenAWSEnvCredentials(presentEnvVars);

      awsConnectShouldThrowsMissingEnvVarError(missingEnvVars[0]);
    });
    const givenAWSCredentialsRemovedFromEnv = envVars =>
      removeAWSCredentialsFromEnv(envVars);
    const awsConnectShouldThrowsMissingEnvVarError = envVar => {
      expect(() => AWSClient.connect()).toThrow(new missingEnvVarError(envVar));
    };
  });

  it('should not throw when all AWS mandatory environment variables are present', async () => {
    expect.hasAssertions();
    givenAWSEnvCredentials([
      'AWS_ACCESS_KEY_ID',
      'AWS_SECRET_ACCESS_KEY',
      'AWS_REGION',
    ]);

    awsConnectDontThrowMissingEnvVarError();
  });
  const givenAWSEnvCredentials = envVars => {
    envVars.forEach(envVar => (process.env[envVar] = envVar.toLowerCase()));
  };
  const awsConnectDontThrowMissingEnvVarError = () => {
    expect(() => AWSClient.connect()).not.toThrow();
  };
});

const removeAWSCredentialsFromEnv = envVars => {
  envVars.forEach(envVar => delete process.env[envVar]);
};
