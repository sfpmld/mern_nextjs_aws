const { AWSClient } = require('./awsClient');
const { AWS_SES_VERSION } = require('../../../constants');

class AWSSESClient {
  constructor() {
    this.awsClient = AWSClient.connect();
    this.sesClient;
  }

  init() {
    this.sesClient = new this.awsClient.SES({
      apiVersion: AWS_SES_VERSION,
    });
    return this.sesClient;
  }

  send(params) {
    this.sesClient.sendEmail(params);
  }
}

module.exports = { AWSSESClient };
