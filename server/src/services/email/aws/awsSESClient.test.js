const { AWS_SES_VERSION } = require('../../../constants');
const { AWSSESClient } = require('./awsSESClient');

const mockAWSSES = jest.fn();
jest.mock('./awsClient', () => ({
  AWSClient: {
    connect: jest.fn(() => ({
      SES: mockAWSSES,
    })),
  },
}));
const emailParams = 'emailParams';

describe('AWSSESClient', () => {
  const expectedSESConfig = { apiVersion: AWS_SES_VERSION };

  describe('init', () => {
    it(`should initialize client with ${JSON.stringify(
      expectedSESConfig
    )}`, async () => {
      expect.hasAssertions();
      mockAWSSES.mockImplementation(() => 'ok');

      givenSESInitialized();
      thenAWSClientShouldBeCalledWith(expectedSESConfig);
    });
  });
  describe('send', () => {
    it(`should call sendEmail with "${emailParams}"`, async () => {
      expect.hasAssertions();
      const mockSendEmail = jest.fn();
      mockSendEmail.mockImplementation(() => 'okk');
      mockAWSSES.mockImplementation(() => ({ sendEmail: mockSendEmail }));

      givenSESSentEmail();
      thenSESClientShouldBeCalledWith(emailParams);
    });
  });

  const givenSESInitialized = () => {
    const awsSESClient = new AWSSESClient();
    awsSESClient.init();
  };
  const givenSESSentEmail = () => {
    const awsSESClient = new AWSSESClient();
    awsSESClient.init();
    awsSESClient.send(emailParams);
  };

  const thenAWSClientShouldBeCalledWith = SESConfig => {
    expect(mockAWSSES).toHaveBeenCalledWith(SESConfig);
  };
  const thenSESClientShouldBeCalledWith = sesParams => {
    expect(mockAWSSES(sesParams).sendEmail).toHaveBeenCalledWith(emailParams);
  };
});
