const { AWSClient } = require('./awsClient');
const { AWSSESClient } = require('./awsSESClient');

module.exports = {
  AWSClient,
  AWSSESClient,
};
