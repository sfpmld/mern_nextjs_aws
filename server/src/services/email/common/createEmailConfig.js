const { EMAIL_FROM, EMAIL_TO } = require('../../../constants');
const { makeFormatEmailMessage } = require('./formatEmailMessage');

const createEmailConfig = async ({
  email,
  subject,
  data,
  messageFormatter,
}) => {
  const formatEmailMessage = makeFormatEmailMessage(messageFormatter);

  return {
    Source: EMAIL_FROM,
    Destination: {
      ToAddress: [email],
    },
    ReplyToAddresses: [EMAIL_TO],
    Message: {
      ...(await formatEmailMessage(subject, data)),
    },
  };
};

module.exports = { createEmailConfig };
