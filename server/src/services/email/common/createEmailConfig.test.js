const { createEmailConfig } = require('./createEmailConfig');
const {
  createSimpleMessageBody,
} = require('../simpleMessage/createSimpleMessageBody');
const { makeFormatEmailMessage } = require('./formatEmailMessage');
jest.mock('./formatEmailMessage', () => {
  const originalModule = jest.requireActual('./formatEmailMessage');

  return {
    ...originalModule,
    makeFormatEmailMessage: jest.fn(originalModule.makeFormatEmailMessage),
  };
});
const formatEmailMessage = makeFormatEmailMessage(createSimpleMessageBody);
const { EMAIL_FROM, EMAIL_TO } = require('../../../constants');

describe('Service > Email', () => {
  describe('createEmailConfig', () => {
    const email = 'email';
    const subject = 'subject';
    const data = {
      message: 'message',
      otherField: 'other field',
    };

    let emailConfigKeys;
    let emailConfigDestinationKeys;
    let emailConfigMessageKeys;
    let expectedEmailConfig;

    beforeAll(async () => {
      const messageBody = await formatEmailMessage(subject, data);
      expectedEmailConfig = {
        Source: EMAIL_FROM,
        Destination: {
          ToAddress: [email],
        },
        ReplyToAddresses: [EMAIL_TO],
        Message: {
          ...messageBody,
        },
      };
      emailConfigKeys = Object.keys(expectedEmailConfig);
      emailConfigDestinationKeys = Object.keys(expectedEmailConfig.Destination);
      emailConfigMessageKeys = Object.keys(expectedEmailConfig.Message);
    });

    let emailConfig;
    beforeEach(async () => {
      emailConfig = await createEmailConfig({
        email,
        subject,
        data,
        messageFormatter: createSimpleMessageBody,
      });
    });
    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should return an object', async () => {
      expect(typeof emailConfig).toBe('object');
    });

    it(`should return an object with these keys ${emailConfigKeys}`, async () => {
      expect.hasAssertions();
      objectHaveSameKeys(emailConfig, emailConfigKeys);
    });

    describe('Destination', () => {
      it(`should return an object with these keys ${emailConfigDestinationKeys}`, async () => {
        expect.hasAssertions();
        objectHaveSameKeys(emailConfig.Destination, emailConfigDestinationKeys);
      });
    });

    describe('Message', () => {
      it('should call formatEmailMessage function', async () => {
        const spyFormatEmailMessage = jest.fn();
        spyFormatEmailMessage.mockReturnValueOnce('ok');
        const spyMakeFormatEmailMessage = jest.fn(() => spyFormatEmailMessage);
        makeFormatEmailMessage.mockImplementationOnce(
          spyMakeFormatEmailMessage
        );

        emailConfig = createEmailConfig({
          email,
          subject,
          data,
          messageFormatter: createSimpleMessageBody,
        });

        expect(spyFormatEmailMessage).toHaveBeenCalledTimes(1);
      });
      it(`should return an object with these keys ${emailConfigMessageKeys}`, async () => {
        expect.hasAssertions();
        objectHaveSameKeys(emailConfig.Message, emailConfigMessageKeys);
      });
    });

    it('should return an Object corresponding to the SES Configuration Datastructure Object', async () => {
      expect(emailConfig).toEqual(
        expect.objectContaining({
          ...expectedEmailConfig,
        })
      );
    });
  });
});

const objectHaveSameKeys = (obj, keys) => {
  return expect(Object.keys(obj)).toEqual(keys);
};
