const { emailCharset } = require('../../../constants');

const makeFormatEmailMessage = messageFormatter => {
  return async (subject, data) => {
    return {
      Body: {
        Html: {
          Charset: emailCharset,
          Data: await messageFormatter(data),
        },
      },
      Subject: {
        Charset: emailCharset,
        Data: subject,
      },
    };
  };
};

module.exports = { makeFormatEmailMessage };
