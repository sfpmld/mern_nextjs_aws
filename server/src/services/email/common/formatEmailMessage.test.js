const { makeFormatEmailMessage } = require('./formatEmailMessage');
const {
  createSimpleMessageBody,
} = require('../simpleMessage/createSimpleMessageBody');
const { emailCharset } = require('../../../constants');

const formatEmailMessage = makeFormatEmailMessage(createSimpleMessageBody);
const message = 'message';
const subject = 'subject';
const expectedFormatedMessage = {
  Body: {
    Html: {
      Charset: emailCharset,
      Data: message,
    },
  },
  Subject: {
    Charset: emailCharset,
    Data: subject,
  },
};

const formatedMessageKeys = Object.keys(expectedFormatedMessage);
const formatedMessageBodyKeys = Object.keys(expectedFormatedMessage.Body);
const formatedMessageBodyHtmlKeys = Object.keys(
  expectedFormatedMessage.Body.Html
);
const formatedMessageSubjectKeys = Object.keys(expectedFormatedMessage.Subject);

describe('SERVICE > EMAIL', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('formatEmailMessage', () => {
    let formatedMessage;
    beforeEach(async () => {
      formatedMessage = await formatEmailMessage('subject', 'message');
    });

    it('should return an object', async () => {
      expect(typeof formatedMessage).toBe('object');
    });

    it(`this object should have keys: "${formatedMessageKeys}"`, async () => {
      expect(Object.keys(formatedMessage)).toEqual(formatedMessageKeys);
    });

    describe('Body', () => {
      it(`should have keys: "${formatedMessageBodyKeys}"`, async () => {
        expect(Object.keys(formatedMessage.Body)).toEqual(
          formatedMessageBodyKeys
        );
      });
      describe('Html', () => {
        it(`should have keys: "${formatedMessageBodyHtmlKeys}"`, async () => {
          expect(Object.keys(formatedMessage.Body.Html)).toEqual(
            formatedMessageBodyHtmlKeys
          );
        });
        describe('Charset', () => {
          it(`should be ${emailCharset}`, async () => {
            expect(formatedMessage.Subject.Charset).toEqual(
              expect.stringContaining(emailCharset)
            );
          });
        });
        describe('Data', () => {
          it(`should contain ${message}`, async () => {
            expect(formatedMessage.Body.Html.Data).toEqual(
              expect.stringContaining(message)
            );
          });
        });
      });
    });

    describe('Subject', () => {
      it(`should have keys: ${formatedMessageSubjectKeys}`, async () => {
        expect(Object.keys(formatedMessage.Subject)).toEqual(
          formatedMessageSubjectKeys
        );
      });
      describe('Charset', () => {
        it(`should be ${emailCharset}`, async () => {
          expect(formatedMessage.Subject.Charset).toEqual(
            expect.stringContaining(emailCharset)
          );
        });
      });
      describe('Data', () => {
        it(`should contain ${subject}`, async () => {
          expect(formatedMessage.Subject.Data).toEqual(
            expect.stringContaining(subject)
          );
        });
      });
    });

    it('should correspond to formattedEmailMessage DataStructure', async () => {
      expect(formatedMessage).toEqual(
        expect.objectContaining({
          Body: {
            Html: {
              Charset: emailCharset,
              Data: expect.stringContaining(message),
            },
          },
          Subject: {
            Charset: emailCharset,
            Data: subject,
          },
        })
      );
    });
  });
});
