const fs = require('fs').promises;
const { join } = require('path');
const { tryAsyncAwait } = require('../../../helpers');
const { internalError } = require('../../../errors');
const { MISSING_TEMPLATE_ERROR_MESSAGE } = require('../../../constants');

class messageFormatter {
  constructor(template, data, templateRenderer) {
    this.template = template;
    this.data = data;
    this.templateBuffer;
    this.templateRenderer = templateRenderer;
  }

  render() {
    return this.templateRenderer(this.templateBuffer, this.data);
  }

  getTemplate() {
    const path = this.getTemplatePath(this.template);
    return this.readTemplate(path);
  }

  getTemplatePath(name) {
    return join(__dirname, `../templates/${name}`);
  }

  async readTemplate(path) {
    const [err, res] = await tryAsyncAwait(fs.readFile(path));

    this.throwsIfFileIsMissing(err);
    this.templateBuffer = res;
  }

  throwsIfFileIsMissing(err) {
    if (err && err.code == 'ENOENT')
      throw new internalError(MISSING_TEMPLATE_ERROR_MESSAGE);
    if (err) throw err;
  }
}

module.exports = { messageFormatter };
