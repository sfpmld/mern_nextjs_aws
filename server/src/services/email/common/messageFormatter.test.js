const { messageFormatter } = require('./messageFormatter');
const fs = require('fs').promises;
const { join } = require('path');
const { templateRenderer } = require('../../../helpers');
const { internalError } = require('../../../errors');
const { MISSING_TEMPLATE_ERROR_MESSAGE } = require('../../../constants');
jest.mock('../../../helpers/logger');

describe('messageFormatter', () => {
  const mockFSRead = jest.spyOn(fs, 'readFile');

  let templateBuffer;
  const template = 'template.html';
  const templateContent = 'template content with {{ message }} anchors';
  const missingTemplateError = new Error(MISSING_TEMPLATE_ERROR_MESSAGE);
  missingTemplateError.code = 'ENOENT';
  const anotherError = new Error('another error');

  const message = 'message';
  const data = { message };
  let formattedMessage;
  const formatter = new messageFormatter(template, data, templateRenderer);

  beforeEach(async () => {
    mockFSRead.mockResolvedValue(templateContent);
    templateBuffer = await formatter.getTemplate();
    formattedMessage = await formatter.render();
  });
  afterEach(() => {
    mockFSRead.mockClear();
  });

  describe('getTemplate method', () => {
    it('should be defined', async () => {
      expect(typeof formatter.getTemplate).toBe('function');
    });

    it('should read template file via readFile fs method called with template path', async () => {
      const path = join(__dirname, `../templates/${template}`);
      expect(mockFSRead).toHaveBeenCalledWith(path);
    });

    it('should update templateBuffer property with template file content', async () => {
      await formatter.getTemplate();
      expect(formatter.templateBuffer).toBe(templateContent);
    });

    it(`should reject an internalError("${MISSING_TEMPLATE_ERROR_MESSAGE}") when template is not found`, async () => {
      mockFSRead.mockClear();
      mockFSRead.mockRejectedValue(missingTemplateError);

      templateBuffer = formatter.getTemplate(template);
      await expect(templateBuffer).rejects.toEqual(missingTemplateError);
      await expect(templateBuffer).rejects.toEqual(
        new internalError(MISSING_TEMPLATE_ERROR_MESSAGE)
      );
    });

    it('should reject another Error than missingTemplateError if error with err.code != "ENOENT"', async () => {
      mockFSRead.mockClear();
      mockFSRead.mockRejectedValue(anotherError);

      templateBuffer = formatter.getTemplate(template);
      await expect(templateBuffer).rejects.not.toEqual(missingTemplateError);
      await expect(templateBuffer).rejects.toEqual(anotherError);
    });
  });

  describe('render method', () => {
    it('should be defined', async () => {
      expect(typeof formatter.render).toBe('function');
    });

    it(`should return a string containing "${Object.values(
      data
    )}"`, async () => {
      Object.values(data).forEach(d =>
        expect(formattedMessage).toEqual(expect.stringContaining(d))
      );
    });
  });
});
