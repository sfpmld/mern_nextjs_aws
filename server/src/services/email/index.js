const { sendEmail } = require('./sendEmail');
const { AWSClient, AWSSESClient } = require('./aws');
const { createEmailConfig } = require('./common/createEmailConfig');
const { makeFormatEmailMessage } = require('./common/formatEmailMessage');
const {
  accountActivationMessageFormatter,
  createAccountActivationEmailConfig,
} = require('./accountActivation/createAccountActivationEmailConfig');

const emailSender = new sendEmail(AWSSESClient).init();

module.exports = {
  AWSClient,
  AWSSESClient,
  sendEmail: emailSender,
  createEmailConfig,
  makeFormatEmailMessage,
  accountActivationMessageFormatter,
  createAccountActivationEmailConfig,
};
