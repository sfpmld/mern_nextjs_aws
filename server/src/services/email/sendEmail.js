const { logger } = require('../../helpers');
const { ACTIVE_SEND_EMAIL } = require('../../constants');

class sendEmail {
  constructor(client) {
    this.clientInstance = new client();
  }

  init() {
    this.clientInstance = this.clientInstance.init();
    return this;
  }

  send(emailParams) {
    return ACTIVE_SEND_EMAIL === 'true'
      ? this.clientInstance.send(emailParams)
      : logger.info('email sent!');
  }
}

module.exports = { sendEmail };
