const { sendEmail } = require('./sendEmail');
const { AWSSESClient } = require('./aws');
jest.mock('./aws/awsSESClient');
jest.mock('../../helpers/logger');

describe('Service > Email', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  describe('sendEmail', () => {
    const emailConfig = 'emailConfig';
    let sendEmailHandler;
    beforeEach(async () => {
      sendEmailHandler = new sendEmail(AWSSESClient);
    });

    describe('constructor', () => {
      it('should create (an AWS SES) Client instance', async () => {
        expect(AWSSESClient).toHaveBeenCalledTimes(1);
      });
    });

    describe('init method', () => {
      it('should call init method of (AWS SES) Client instance', async () => {
        const spyInit = AWSSESClient.mock.instances[0].init;
        sendEmailHandler.init();
        expect(spyInit).toHaveBeenCalledTimes(1);
      });
    });

    describe('send', () => {
      it(`should call send method of (AWS SES) Client instance with "${emailConfig}"`, async () => {
        const spySend = AWSSESClient.mock.instances[0].send;
        sendEmailHandler.send(emailConfig);
        expect(spySend).toHaveBeenCalledTimes(1);
        expect(spySend).toHaveBeenCalledWith(emailConfig);
      });
    });
  });
});
