const createSimpleMessageBody = data =>
  `<html><body>${JSON.stringify(data)}</body></html>`;

module.exports = { createSimpleMessageBody };
