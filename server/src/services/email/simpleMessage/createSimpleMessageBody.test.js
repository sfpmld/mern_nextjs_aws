const { createSimpleMessageBody } = require('./createSimpleMessageBody');

const data = {
  message: 'message',
  otherField: 'other stuff',
};

describe('createSimpleMessageBody Message Formatter', () => {
  let messageBody;
  beforeEach(() => {
    messageBody = createSimpleMessageBody(data);
  });

  it('should render a string', async () => {
    expect(typeof messageBody).toBe('string');
  });
  it(`should render a string containing ${Object.keys(data)}`, async () => {
    Object.values(data).forEach(d =>
      expect(messageBody).toEqual(expect.stringContaining(d))
    );
  });
});
