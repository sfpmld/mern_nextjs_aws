const { createSimpleMessageBody } = require('./createSimpleMessageBody');

module.exports = {
  createSimpleMessageBody,
};
